/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.dao;

import com.waraporn.ngoproject.databasehelper.DatabaseHelper;
import com.waraporn.ngoproject.model.MatHistory;
import com.waraporn.ngoproject.model.MatHistoryDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class MatHistoryDao implements Dao<MatHistory> {

    @Override
    public MatHistory get(int id) {
        MatHistory matHis = null;
        String sql = "SELECT * FROM checkMatHistory WHERE cmh_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                matHis = MatHistory.fromRS(rs);
                MatHistoryDetailDao mhd = new MatHistoryDetailDao();
                ArrayList<MatHistoryDetail> matHisDetails
                        = (ArrayList<MatHistoryDetail>) mhd.getAll("cmh_id=" + matHis.getId(), " cmh_detail_id");
                matHis.setMatHistoryDetails(matHisDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return matHis;
    }

    @Override
    public List<MatHistory> getAll() {
        ArrayList<MatHistory> list = new ArrayList();
        String sql = "SELECT * FROM checkMatHistory";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MatHistory matHis = MatHistory.fromRS(rs);
                list.add(matHis);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<MatHistory> getAll(String where, String order) {
        ArrayList<MatHistory> list = new ArrayList();
        String sql = "SELECT * FROM checkMatHistory where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MatHistory matHis = MatHistory.fromRS(rs);
                list.add(matHis);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<MatHistory> getAll(String order) {
        ArrayList<MatHistory> list = new ArrayList();
        String sql = "SELECT * FROM checkMatHistory  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MatHistory matHis = MatHistory.fromRS(rs);
                list.add(matHis);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public MatHistory save(MatHistory obj) {
        String sql = "INSERT INTO checkMatHistory (user_id)"
                + "VALUES(?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUserId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public MatHistory update(MatHistory obj) {
        String sql = "UPDATE checkMatHistory"
                + " SET user_id = ?"
                + " WHERE cmh_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUserId());
            stmt.setInt(2, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(MatHistory obj) {
        String sql = "DELETE FROM checkMatHistory WHERE cmh_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
