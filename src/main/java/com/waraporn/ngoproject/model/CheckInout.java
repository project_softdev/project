/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class CheckInout {

    private int id;
    private String date;
    private String checkIn;
    private String checkOut;
    private int totalHour;
    private int empId;
    private Employee employee;

    public CheckInout(int id, String date, String checkIn, String checkOut, int totalHour, int empId) {
        this.id = id;
        this.date = date;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.totalHour = totalHour;
        this.empId = empId;
    }

    public CheckInout(String date, String checkIn, String checkOut, int totalHour, int empId) {
        this.id = -1;
        this.date = date;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.totalHour = totalHour;
        this.empId = empId;
    }

    public CheckInout() {
        this.id = -1;
        this.date = "";
        this.checkIn = "";
        this.checkOut = "";
        this.totalHour = 0;
        this.empId = 0;
    }

    public CheckInout(int id, String checkOut, int totalHour, int empId) {
        this.id = id;
        this.checkOut = checkOut;
        this.totalHour = totalHour;
        this.empId = empId;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public int getTotalHour() {
        return totalHour;
    }

    public void setTotalHour(int totalHour) {
        this.totalHour = totalHour;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.empId = employee.getId();

    }

    @Override
    public String toString() {
        return "CheckInout{" + "id=" + id + ", date=" + date + ", checkIn=" + checkIn + ", checkOut=" + checkOut + ", totalHour=" + totalHour + ", empId=" + empId + ", employee=" + employee + '}';
    }

    public static CheckInout fromRS(ResultSet rs) {
        CheckInout checkinout = new CheckInout();
        try {
            checkinout.setId(rs.getInt("check_id"));
            checkinout.setDate(rs.getString("date"));
            checkinout.setCheckIn(rs.getString("check_in"));
            checkinout.setCheckOut(rs.getString("check_out"));
            checkinout.setTotalHour(rs.getInt("total_hour"));
            checkinout.setEmpId(rs.getInt("emp_id"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckInout.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkinout;
    }

}
