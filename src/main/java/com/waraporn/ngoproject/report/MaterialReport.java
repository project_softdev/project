/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class MaterialReport {
    private int id;
    private String name;
    private int min;
    private int oldQty;
    private int qty;
    private int usedQty;
    private String status;

    public MaterialReport(int id, String name, int min, int oldQty, int qty, int usedQty, String status) {
        this.id = id;
        this.name = name;
        this.min = min;
        this.oldQty = oldQty;
        this.qty = qty;
        this.usedQty = usedQty;
        this.status = status;
    }
    
    public MaterialReport(String name, int min, int oldQty, int qty, int usedQty, String status) {
        this.id = -1;
        this.name = name;
        this.min = min;
        this.oldQty = oldQty;
        this.qty = qty;
        this.usedQty = usedQty;
        this.status = status;
    }
    
    public MaterialReport() {
        this.id = -1;
        this.name = "";
        this.min = 0;
        this.oldQty = 0;
        this.qty = 0;
        this.usedQty = 0;
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getOldQty() {
        return oldQty;
    }

    public void setOldQty(int oldQty) {
        this.oldQty = oldQty;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getUsedQty() {
        return usedQty;
    }

    public void setUsedQty(int usedQty) {
        this.usedQty = usedQty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MaterialReport{" + "id=" + id + ", name=" + name + ", min=" + min + ", oldQty=" + oldQty + ", qty=" + qty + ", usedQty=" + usedQty + ", status=" + status + '}';
    }

    
     
    public static MaterialReport fromRS(ResultSet rs) {
        MaterialReport materialReport = new MaterialReport();
        try {
            materialReport.setId(rs.getInt("mat_id"));           
            materialReport.setName(rs.getString("mat_name"));
            materialReport.setMin(rs.getInt("mat_min"));
            materialReport.setOldQty(rs.getInt("OldQuantity"));
            materialReport.setQty(rs.getInt("mat_quantity"));
            materialReport.setUsedQty(rs.getInt("UsedQuantity"));
            materialReport.setStatus(rs.getString("mat_status"));
            
        } catch (SQLException ex) {
            Logger.getLogger(MaterialReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return materialReport;
    }    
}
