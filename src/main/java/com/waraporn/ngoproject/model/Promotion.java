/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class Promotion {

    private int Id;
    private String proName;
    private int proDiscount;

    public Promotion(int Id, String proName, int proDiscount) {
        this.Id = Id;
        this.proName = proName;
        this.proDiscount = proDiscount;
    }

    public Promotion(String proName, int proDiscount) {
        this.Id = -1;
        this.proName = proName;
        this.proDiscount = proDiscount;
    }

    public Promotion() {
        this.Id = -1;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public int getProDiscount() {
        return proDiscount;
    }

    public void setProDiscount(int proDiscount) {
        this.proDiscount = proDiscount;
    }

    @Override
    public String toString() {
        return "Promotion{" + "Id=" + Id + ", proName=" + proName + ", proDiscount=" + proDiscount + '}';
    }

   
    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("promotion_id"));
            promotion.setProName(rs.getString("promotion_name"));
            promotion.setProDiscount(rs.getInt("promotion_discount"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }
    
}
