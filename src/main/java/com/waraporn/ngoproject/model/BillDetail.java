/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import com.waraporn.ngoproject.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class BillDetail {

    private int id;
    private int matId;
    private String matName;
    private int qty;
    private float price;
    private float totalPrice;
    private int billId;
    private Bill bill;
    private Material material;
//    private ArrayList<BillDetail> billDetails = new ArrayList<BillDetail>();

    public BillDetail(int id, int matId, String matName, int qty, float price, float totalPrice, int billId) {
        this.id = id;
        this.matId = matId;
        this.matName = matName;
        this.qty = qty;
        this.price = price;
        this.totalPrice = totalPrice;
        this.billId = billId;

    }

    public BillDetail(int matId, String matName, int qty, float price, float totalPrice, int billId) {
        this.id = -1;
        this.matId = matId;
        this.matName = matName;
        this.qty = qty;
        this.price = price;
        this.totalPrice = totalPrice;
        this.billId = billId;
    }
    
    public BillDetail(String matName, int qty, float price, float totalPrice) {
        this.id = -1;
        this.matId = 0;
        this.matName = matName;
        this.qty = qty;
        this.price = price;
        this.totalPrice = totalPrice;
        this.billId = 0;
    }
    
    public BillDetail(String matName, int qty) {
        this.id = -1;
        this.matId = 0;
        this.matName = matName;
        this.qty = qty;
        this.price = 0.0f;
        this.totalPrice = 0.0f;
        this.billId = 0;
    }

    public BillDetail() {
        this.id = -1;
        this.matId = 0;
        this.matName = "";
        this.qty = 0;
        this.price = 0.0f;
        this.totalPrice = 0.0f;
        this.billId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatId() {
        return matId;
    }

    public void setMatId(int matId) {
        this.matId = matId;
    }

    public String getMatName() {
        return matName;
    }

    public void setMatName(String matName) {
        this.matName = matName;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        totalPrice = qty*price;
    }
    

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "BillDetail{" + "id=" + id + ", matId=" + matId + ", matName=" + matName + ", qty=" + qty + ", price=" + price + ", totalPrice=" + totalPrice + ", billId=" + billId + ", bill=" + bill + ", material=" + material + '}';
    }   

    public static BillDetail fromRS(ResultSet rs) {
        BillDetail billDetail = new BillDetail();
        try {
            billDetail.setId(rs.getInt("bill_detail_id"));
            billDetail.setMatId(rs.getInt("mat_id"));
            billDetail.setMatName(rs.getString("mat_name"));
            billDetail.setQty(rs.getInt("bill_detail_qty"));
            billDetail.setPrice(rs.getFloat("bill_detail_price"));
            billDetail.setTotalPrice(rs.getFloat("bill_detail_total_price"));
            billDetail.setBillId(rs.getInt("bill_id"));
            
            MaterialDao materialDao = new MaterialDao();
            Material material = materialDao.get(rs.getInt("mat_id"));
            billDetail.setMaterial(material);

        } catch (SQLException ex) {
            Logger.getLogger(BillDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billDetail;
    }

    
}
