/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package test_Service;

import com.waraporn.ngoproject.model.Customer;
import com.waraporn.ngoproject.service.CustomerService;

/**
 *
 * @author ACER
 */
public class TestCustomerService {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        System.out.println("getAll");
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
        // getByTel
        System.out.println("getByTel");
        System.out.println(cs.getByTel("0947751243"));
        
        // add new customer
//        Customer cus1 = new Customer("หมิง", "0909329910");
//        cs.addNew(cus1);
//        System.out.println("add new");
//        for(Customer customer : cs.getCustomers()){
//            System.out.println(customer);
//        }

            
        // update , delete customer
        Customer delCus = cs.getByTel("0909329910");
        delCus.setTel("0909329911");
        cs.update(delCus);
        System.out.println("After Updated");
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
        
        cs.delete(delCus);
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
            
    }
    
}
