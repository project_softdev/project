/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import com.waraporn.ngoproject.dao.BillDetailDao;
import com.waraporn.ngoproject.dao.EmployeeDao;
import com.waraporn.ngoproject.dao.ExpensesDetailDao;
import com.waraporn.ngoproject.dao.MaterialDao;
import com.waraporn.ngoproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class Expenses {
    private int id;
    private Date date;
    private float pay;
    private int userId;
    private User user;
    private ArrayList<ExpensesDetail> expensesDetails = new ArrayList();

    public Expenses(int id, Date date, float pay, int userId) {
        this.id = id;
        this.date = date;
        this.pay = pay;
        this.userId = userId;
    }
    
    public Expenses(Date date, float pay, int userId) {
        this.id = -1;
        this.date = date;
        this.pay = pay;
        this.userId = userId;
    }
    
    public Expenses(float pay, int userId) {
        this.id = -1;
        this.date = null;
        this.pay = pay;
        this.userId = userId;
    }
    
    public Expenses(int userId) {
        this.id = -1;
        this.date = null;
        this.pay = 0.0f;
        this.userId = userId;
    }
    
    public Expenses() {
        this.id = -1;
        this.date = null;
        this.pay = 0.0f;
        this.userId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getPay() {
        return pay;
    }

    public void setPay(float pay) {
        this.pay = pay;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }

    public ArrayList<ExpensesDetail> getExpensesDetails() {
        return expensesDetails;
    }

    public void setExpensesDetails(ArrayList expensesDetails) {
        this.expensesDetails = expensesDetails;
    }

    @Override
    public String toString() {
        return "Expenses{" + "id=" + id + ", date=" + date + ", pay=" + pay + ", userId=" + userId + ", user=" + user + ", expensesDetails=" + expensesDetails + '}';
    }
    
    public void addExpensesDetail(String name, Float price) {
        ExpensesDetail ed = new ExpensesDetail(name, price);
        expensesDetails.add(ed);
        calculateTotal();
    }

    public void delExpensesDetail() {
        expensesDetails.clear();
        calculateTotal();
    }

    public void calculateTotal() {
        float price = 0.0f;
        for (ExpensesDetail ed : expensesDetails) {
            price += ed.getPrice();
        }
        this.pay = price;
    }
    
    public static Expenses fromRS(ResultSet rs) {
        Expenses expenses = new Expenses();
        try {
            expenses.setId(rs.getInt("expenses_id"));
            expenses.setDate(rs.getTimestamp("expenses_date"));
            expenses.setPay(rs.getFloat("expenses_pay"));
            expenses.setUserId(rs.getInt("user_id"));
            
            UserDao userDao = new UserDao();
            User user = userDao.get(expenses.getUserId());
            expenses.setUser(user);

            ExpensesDetailDao expensesDetailDao = new ExpensesDetailDao();
            expenses.setExpensesDetails((ArrayList<ExpensesDetail>) expensesDetailDao.getByCheckExpensesId(expenses.getId()));
        
        } catch (SQLException ex) {
            Logger.getLogger(Expenses.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return expenses;
    }
}
