/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.SummarySalaryDao;
import com.waraporn.ngoproject.model.SummarySalary;
import java.util.List;

/**
 *
 * @author ACER
 */
public class SummarySalaryService {

   public SummarySalary getById(int id) {
        SummarySalaryDao summarySalaryDao = new SummarySalaryDao();
        return summarySalaryDao.get(id);
    }

    public List<SummarySalary> getSummarySalarys() {
        SummarySalaryDao summarySalaryDao = new SummarySalaryDao();
        return summarySalaryDao.getAll(" ss_id asc");
    }

    public SummarySalary addNew(SummarySalary editedSummarySalary) {
        SummarySalaryDao summarySalaryDao = new SummarySalaryDao();
        return summarySalaryDao.save(editedSummarySalary);
    }

    public SummarySalary update(SummarySalary editedSummarySalary) {
        SummarySalaryDao summarySalaryDao = new SummarySalaryDao();
        return summarySalaryDao.update(editedSummarySalary);
    }

    public int delete(SummarySalary editedSummarySalary) {
        SummarySalaryDao summarySalaryDao = new SummarySalaryDao();
        return summarySalaryDao.delete(editedSummarySalary);
    }
}
