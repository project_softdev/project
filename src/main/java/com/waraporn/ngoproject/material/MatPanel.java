/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.waraporn.ngoproject.material;

import com.waraporn.ngoproject.model.MatHistory;
import com.waraporn.ngoproject.model.Material;
import com.waraporn.ngoproject.service.MaterialService;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author DELL
 */
public class MatPanel extends javax.swing.JPanel {

    private final MaterialService materialService;
    private List<Material> list;
    private Material editedMaterial;
    private MatHistory editedMatHistory;

    /**
     * Creates new form MatPanel
     */
    public MatPanel() {
        initComponents();
        materialService = new MaterialService();

        list = materialService.getMaterials();
        initMaterialTable();
        initShowStatusTable();
    }

    private void initShowStatusTable() {
        tblMin.setRowHeight(30);
        tblMin.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 18));
        tblMin.setModel(new AbstractTableModel() {
            String[] columnNames = {"ชื่อ", "สถานะในคลัง"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material material = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        if (material.getQty() == 0) {
                            tblMin.setForeground(Color.RED);
                            return material.getName();
                        } else if (material.getQty() > 0 && material.getQty() < material.getMin()) {
                            tblMin.setForeground(Color.BLACK);
                            return material.getName();
                        } else {
                            tblMin.setForeground(Color.BLACK);
                            return material.getName();
                        }
                    case 1:
                        if (material.getQty() == 0) {
                            tblMin.setForeground(Color.RED);
                            return "หมด";
                        } else if (material.getQty() > 0 && material.getQty() < material.getMin()) {
                            tblMin.setForeground(Color.ORANGE);
                            return "ใกล้หมด";
                        } else {
                            tblMin.setForeground(Color.GREEN);
                            return "คงเหลือ";
                        }
                    default:
                        return "Unknown";
                }
            }
        });
        refreshTable();
    }

    private void initMaterialTable() {
        tblMat.setRowHeight(100);
        tblMat.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 20));
        tblMat.setModel(new AbstractTableModel() {
            String[] columnNames = {"รหัส", "รูปภาพ", "ชื่อ", "จำนวนขั้นต่ำ", "จำนวนคงเหลือ", "หน่วย", "ราคาต่อหน่วย"};
            
            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }
            
            @Override
            public int getRowCount() {
                return list.size();
            }
            
            @Override
            public int getColumnCount() {
                return 7;
            }
            
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 1:
                        return ImageIcon.class;
                    default:
                        return String.class;
                }
            }
            
            
            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material material = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return material.getId();
                    case 1:
                        ImageIcon icon = new ImageIcon("./mat" + material.getId() + ".png");
                        Image image = icon.getImage();
                        int width = image.getWidth(null);
                        int height = image.getHeight(null);
                        Image newImage = image.getScaledInstance((int) (100*(float)width/height), 100, Image.SCALE_SMOOTH);
                        icon.setImage(newImage);
                        return icon;
                    case 2:
                        return material.getName();
                    case 3:
                        return material.getMin();
                    case 4:
                        return material.getQty();
                    case 5:
                        return material.getUnit();
                    case 6:
                        return material.getPrice();
                    default:
                        return "Unknown";
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblMin = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        หน้าสินค้า = new javax.swing.JLabel();
        btnSearch = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        btnDelMat = new javax.swing.JButton();
        btnEditMat = new javax.swing.JButton();
        btnAddMat = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        btnUpdateProduct = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblMat = new javax.swing.JTable();

        setPreferredSize(new java.awt.Dimension(832, 654));

        jPanel1.setBackground(new java.awt.Color(224, 201, 171));

        jPanel2.setBackground(new java.awt.Color(163, 129, 102));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("สถานะวัตถุดิบ");

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        tblMin.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblMin.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null}
            },
            new String [] {
                "name", "qty"
            }
        ));
        tblMin.setGridColor(new java.awt.Color(255, 255, 255));
        jScrollPane2.setViewportView(tblMin);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 410, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(31, 31, 31))
        );

        jPanel3.setBackground(new java.awt.Color(224, 201, 171));

        หน้าสินค้า.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        หน้าสินค้า.setText("คลังวัตถุดิบ");

        btnSearch.setBackground(new java.awt.Color(102, 51, 0));
        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnSearch.setText("ค้นหา");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });

        btnDelMat.setBackground(new java.awt.Color(102, 51, 0));
        btnDelMat.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnDelMat.setForeground(new java.awt.Color(255, 255, 255));
        btnDelMat.setText("ลบ");
        btnDelMat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelMatActionPerformed(evt);
            }
        });

        btnEditMat.setBackground(new java.awt.Color(102, 51, 0));
        btnEditMat.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnEditMat.setForeground(new java.awt.Color(255, 255, 255));
        btnEditMat.setText("แก้ไข");
        btnEditMat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditMatActionPerformed(evt);
            }
        });

        btnAddMat.setBackground(new java.awt.Color(102, 51, 0));
        btnAddMat.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnAddMat.setForeground(new java.awt.Color(255, 255, 255));
        btnAddMat.setText("เพิ่ม");
        btnAddMat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddMatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 271, Short.MAX_VALUE)
                .addComponent(btnAddMat, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditMat, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelMat, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(หน้าสินค้า, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(หน้าสินค้า)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnDelMat)
                        .addComponent(btnEditMat)
                        .addComponent(btnAddMat))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSearch)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(224, 201, 171));

        btnUpdateProduct.setBackground(new java.awt.Color(102, 51, 0));
        btnUpdateProduct.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnUpdateProduct.setForeground(new java.awt.Color(255, 255, 255));
        btnUpdateProduct.setText("อัพเดทสินค้า");
        btnUpdateProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateProductActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnUpdateProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnUpdateProduct)
                .addContainerGap(15, Short.MAX_VALUE))
        );

        tblMat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblMat.setGridColor(new java.awt.Color(255, 255, 255));
        jScrollPane1.setViewportView(tblMat);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(1, 1, 1))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnUpdateProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateProductActionPerformed
        editedMaterial = new Material();
        openUpdateDialog();
    }//GEN-LAST:event_btnUpdateProductActionPerformed

    private void btnAddMatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddMatActionPerformed
        openAddDialog();
    }//GEN-LAST:event_btnAddMatActionPerformed

    private void openAddDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        AddMatDialog AddmaterialDialog = new AddMatDialog(frame, materialService);
        AddmaterialDialog.setSize(850, 600);
        AddmaterialDialog.setLocationRelativeTo(this);
        AddmaterialDialog.setVisible(true);
        AddmaterialDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
            
        });
    }

    private void btnEditMatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditMatActionPerformed
        int selectedIndex = tblMat.getSelectedRow();
        if (selectedIndex >= 0) {
            editedMaterial = list.get(selectedIndex);
            openEditDialog();
        }
    }//GEN-LAST:event_btnEditMatActionPerformed

    private void btnDelMatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelMatActionPerformed
        int selectedIndex = tblMat.getSelectedRow();
        if (selectedIndex >= 0) {
            editedMaterial = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                materialService.delete(editedMaterial);
            }
            refreshTable();
        }
    }//GEN-LAST:event_btnDelMatActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        TableRowSorter<AbstractTableModel> sorter = new TableRowSorter<>((AbstractTableModel) tblMat.getModel());       
         tblMat.setRowSorter(sorter);
         tblMin.setRowSorter(sorter);
         sorter.setRowFilter(RowFilter.regexFilter(txtSearch.getText()));
    }//GEN-LAST:event_btnSearchActionPerformed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        TableRowSorter<AbstractTableModel> sorter = new TableRowSorter<>((AbstractTableModel) tblMat.getModel());     
         tblMat.setRowSorter(sorter);
         tblMin.setRowSorter(sorter);
         sorter.setRowFilter(RowFilter.regexFilter(txtSearch.getText()));
    }//GEN-LAST:event_txtSearchKeyReleased

    private void openUpdateDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CheckMaterialDialog materialDialog = new CheckMaterialDialog(frame, editedMaterial);
        materialDialog.setSize(1120, 708);
        materialDialog.setLocationRelativeTo(this);
        materialDialog.setVisible(true);
        materialDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }


    private void openEditDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        EditMatDialog matEditDialog = new EditMatDialog(frame, editedMaterial);
        matEditDialog.setSize(777, 502);
        matEditDialog.setLocationRelativeTo(this);
        matEditDialog.setVisible(true);
        matEditDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }

    private void refreshTable() {
        list = materialService.getMaterials();
        tblMat.revalidate();
        tblMat.repaint();
        tblMin.revalidate();
        tblMin.repaint();
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddMat;
    private javax.swing.JButton btnDelMat;
    private javax.swing.JButton btnEditMat;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnUpdateProduct;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblMat;
    private javax.swing.JTable tblMin;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JLabel หน้าสินค้า;
    // End of variables declaration//GEN-END:variables

}
