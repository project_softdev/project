/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.dao;

import com.waraporn.ngoproject.databasehelper.DatabaseHelper;
import com.waraporn.ngoproject.model.MatHistoryDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class MatHistoryDetailDao implements Dao<MatHistoryDetail>{

    @Override
    public MatHistoryDetail get(int id) {
        MatHistoryDetail cmhDetail = null;
        String sql = "SELECT * FROM checkMatHistory_detail WHERE cmh_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                cmhDetail = MatHistoryDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return cmhDetail;
    }
    
    public List<MatHistoryDetail> getByMatHistoryId(int id) {
        ArrayList<MatHistoryDetail> list = new ArrayList();
        String sql = "SELECT * FROM checkMatHistory_detail WHERE cmh_detail_id=" + id + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MatHistoryDetail matHisDetail = MatHistoryDetail.fromRS(rs);
                list.add(matHisDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<MatHistoryDetail> getByCheckMatHistoryId(int id) {
        ArrayList<MatHistoryDetail> list = new ArrayList();
        String sql = "SELECT * FROM checkMatHistory_detail WHERE cmh_id= " + id + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MatHistoryDetail matHisDetail = MatHistoryDetail.fromRS(rs);
                list.add(matHisDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<MatHistoryDetail> getAll() {
        ArrayList<MatHistoryDetail> list = new ArrayList();
        String sql = "SELECT * FROM checkMatHistory_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MatHistoryDetail cmhDetail = MatHistoryDetail.fromRS(rs);
                list.add(cmhDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<MatHistoryDetail> getAll(String where, String order) {
        ArrayList<MatHistoryDetail> list = new ArrayList();
        String sql = "SELECT * FROM checkMatHistory_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MatHistoryDetail cmhDetail = MatHistoryDetail.fromRS(rs);
                list.add(cmhDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<MatHistoryDetail> getAll(String order) {
        ArrayList<MatHistoryDetail> list = new ArrayList();
        String sql = "SELECT * FROM checkMatHistory_detail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MatHistoryDetail cmhDetail = MatHistoryDetail.fromRS(rs);
                list.add(cmhDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public MatHistoryDetail save(MatHistoryDetail obj) {
        String sql = "INSERT INTO checkMatHistory_detail (mat_name, old_qty, new_qty, used_qty, unit, cmh_id)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getMatName());
            stmt.setInt(2, obj.getOldQty());
            stmt.setInt(3, obj.getNewQty());
            stmt.setInt(4, obj.getUsedQty());
            stmt.setString(5, obj.getUnit());
            stmt.setInt(6, obj.getCmhId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }        
        return obj;
    }

    @Override
    public MatHistoryDetail update(MatHistoryDetail obj) {
        String sql = "UPDATE checkMatHistory_detail"
                + " SET mat_name = ?, old_qty = ?, new_qty = ?, used_qty = ?, unit = ?, cmh_id = ?"
                + " WHERE cmh_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getMatName());
            stmt.setInt(2, obj.getOldQty());
            stmt.setInt(3, obj.getNewQty());
            stmt.setInt(4, obj.getUsedQty());
            stmt.setString(5, obj.getUnit());
            stmt.setInt(6, obj.getCmhId());
            stmt.setInt(7, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(MatHistoryDetail obj) {
        String sql = "DELETE FROM checkMatHistory_detail WHERE cmh_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
