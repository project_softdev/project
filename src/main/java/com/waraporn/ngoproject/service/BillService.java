/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.BillDao;
import com.waraporn.ngoproject.dao.BillDetailDao;
import com.waraporn.ngoproject.model.Bill;
import com.waraporn.ngoproject.model.BillDetail;
import java.util.List;

/**
 *
 * @author ACER
 */
public class BillService {
    public Bill getById(int id) {
        BillDao billDao = new BillDao();
        Bill bill = billDao.get(id);
        return bill;
    }

    public List<Bill> getBills() {
        BillDao billDao = new BillDao();
        return billDao.getAll(" bill_id asc");
    }

    public Bill addNew(Bill editedBill) {
        BillDao billDao = new BillDao();
        BillDetailDao billDetailDao = new BillDetailDao();
        Bill bill = billDao.save(editedBill);
        for(BillDetail bd : editedBill.getBillDetails()){
            bd.setBillId(bill.getId());
            billDetailDao.save(bd);
        }
        return bill;
    }

    public Bill update(Bill editedBill) {
        if(editedBill != null){
            BillDao billDao = new BillDao();
            return billDao.update(editedBill);
        }else {
            System.out.println("Cannot update a null bill.");
            return null;
        }
    }

    public int delete(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.delete(editedBill);
    }
}
