/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.report;


import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class ExpensesReport {

    private int id;
    private String name;
    private float price;
    private float totalPrice_Per_Month;

    public ExpensesReport(int id, String name, float price, float totalPrice_Per_Month) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.totalPrice_Per_Month = totalPrice_Per_Month;
    }

    public ExpensesReport(String name, float price, float totalPrice_Per_Month) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.totalPrice_Per_Month = totalPrice_Per_Month;
    }
    
    public ExpensesReport(String name) {
        this.id = -1;
        this.name = name;
        this.price = 0.0f;
        this.totalPrice_Per_Month = 0.0f;
    }
    
    public ExpensesReport() {
        this.id = -1;
        this.name = "";
        this.price = 0.0f;
        this.totalPrice_Per_Month = 0.0f;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getTotalPrice_Per_Month() {
        return totalPrice_Per_Month;
    }

    public void setTotalPrice_Per_Month(float totalPrice_Per_Month) {
        this.totalPrice_Per_Month = totalPrice_Per_Month;
    }

    @Override
    public String toString() {
        return "ExpensesReport{"  + ", id=" + id + ", name=" + name + ", price=" + price + ", totalPrice_Per_Month=" + totalPrice_Per_Month + '}';
    }
    
    public static ExpensesReport fromRS(ResultSet rs) {
        ExpensesReport expensesReport = new ExpensesReport();
        try {
            expensesReport.setId(rs.getInt("expenses_id"));           
            expensesReport.setName(rs.getString("expenses_detail_name"));
            expensesReport.setPrice(rs.getInt("price"));
            expensesReport.setTotalPrice_Per_Month(rs.getInt("totalPrice_Per_Month"));
            
        } catch (SQLException ex) {
            Logger.getLogger(ExpensesReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return expensesReport;
    }    
}
