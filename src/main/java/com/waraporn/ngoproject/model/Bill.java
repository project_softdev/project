/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import com.waraporn.ngoproject.dao.BillDetailDao;
import com.waraporn.ngoproject.dao.EmployeeDao;
import com.waraporn.ngoproject.dao.MaterialDao;
import com.waraporn.ngoproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class Bill {

    private int id;
    private Date date;
    private String shopName;
    private float subtotal;
    private float discount;
    private float totalPrice;
    private int totalQty;
    private int userId;
    private User user;
    private ArrayList<BillDetail> billDetails = new ArrayList();

    public Bill(int id, Date date, String shopName, float subtotal, float discount, float totalPrice, int totalQty, int userId) {
        this.id = id;
        this.date = date;
        this.shopName = shopName;
        this.subtotal = subtotal;
        this.discount = discount;
        this.totalPrice = totalPrice;
        this.totalQty = totalQty;
        this.userId = userId;
    }

    public Bill(Date date, String shopName, float subtotal, float discount, float totalPrice, int totalQty, int userId) {
        this.id = -1;
        this.date = date;
        this.shopName = shopName;
        this.subtotal = subtotal;
        this.discount = discount;
        this.totalPrice = totalPrice;
        this.totalQty = totalQty;
        this.userId = userId;
    }

    public Bill(String shopName, float subtotal, float discount, float totalPrice, int totalQty, int userId) {
        this.id = -1;
        this.date = null;
        this.shopName = shopName;
        this.subtotal = subtotal;
        this.discount = discount;
        this.totalPrice = totalPrice;
        this.totalQty = totalQty;
        this.userId = userId;
    }

    public Bill(String shopName, int totalQty, int userId) {
        this.id = -1;
        this.date = null;
        this.shopName = shopName;
        this.subtotal = 0.0f;
        this.discount = 0.0f;
        this.totalPrice = 0.0f;
        this.totalQty = totalQty;
        this.userId = userId;
    }

    public Bill() {
        this.id = -1;
        this.date = null;
        this.shopName = "";
        this.subtotal = 0.0f;
        this.discount = 0.0f;
        this.totalPrice = 0.0f;
        this.totalQty = 0;
        this.userId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public float getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(float subtotal) {
        this.subtotal = subtotal;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }

    public ArrayList<BillDetail> getBillDetails() {
        return billDetails;
    }

    public void setBillDetails(ArrayList billDetails) {
        this.billDetails = billDetails;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", date=" + date + ", shopName=" + shopName + ", subtotal=" + subtotal + ", discount=" + discount + ", totalPrice=" + totalPrice + ", totalQty=" + totalQty + ", userId=" + userId + ", user=" + user + ", billDetails=" + billDetails + '}';
    }

//    public void addBillDetail(String matName, int qty, float price, float totalPrice) {
//        BillDetail bd = new BillDetail(matName, qty, price, totalPrice);
//        billDetails.add(bd);
//        calculateTotal();
//    }
    public void addBillDetail(Material mat, float price, int qty) {
        BillDetail bd = new BillDetail(mat.getId(), mat.getName(), qty, price, qty * price, -1);
        billDetails.add(bd);
        calculateTotal();
    }

    public void delBillDetail() {
        billDetails.clear();
        discount = 0;
        calculateTotal();
    }

    public void delBillDetail(BillDetail billDetail) {
        billDetails.remove(billDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float subtotal = 0.0f;
        float totalPrice = 0.0f;
        for (BillDetail bd : billDetails) {
            subtotal += bd.getTotalPrice();
            totalQty += bd.getQty();
        }
        this.totalQty = totalQty;
        this.subtotal = subtotal;
        this.totalPrice = subtotal;
    }

    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("bill_id"));
            bill.setDate(rs.getTimestamp("bill_date"));
            bill.setShopName(rs.getString("bill_shop_name"));
            bill.setSubtotal(rs.getFloat("bill_subtotal"));
            bill.setDiscount(rs.getFloat("bill_discount"));
            bill.setTotalPrice(rs.getFloat("bill_total_price"));
            bill.setTotalQty(rs.getInt("bill_total_qty"));
            bill.setUserId(rs.getInt("user_id"));

            UserDao userDao = new UserDao();
            User user = userDao.get(bill.getUserId());
            bill.setUser(user);

            BillDetailDao billDetailDao = new BillDetailDao();
            bill.setBillDetails((ArrayList<BillDetail>) billDetailDao.getByCheckBillId(bill.getId()));

            MaterialDao materialDao = new MaterialDao();
            for (BillDetail b : bill.getBillDetails()) {
                Material mat = materialDao.get(b.getMatId());
                b.setMaterial(mat);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }
}
