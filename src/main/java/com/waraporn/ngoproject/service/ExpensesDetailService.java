/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.ExpensesDetailDao;
import com.waraporn.ngoproject.model.ExpensesDetail;
import java.util.List;

/**
 *
 * @author DELL
 */
public class ExpensesDetailService {
    public ExpensesDetail getById(int id) {
        ExpensesDetailDao expensesDetailDao = new ExpensesDetailDao();
        ExpensesDetail cmhDetail = expensesDetailDao.get(id);
        return cmhDetail;
    }

    public List<ExpensesDetail> getExpensesDetailsDetail() {
        ExpensesDetailDao expensesDetailDao = new ExpensesDetailDao();
        return expensesDetailDao.getAll(" expenses_detail_id asc");
    }

    public ExpensesDetail addNew(ExpensesDetail editedExpensesDetail) {
        ExpensesDetailDao expensesDetailDao = new ExpensesDetailDao();
        return expensesDetailDao.save(editedExpensesDetail);
    }

    public ExpensesDetail update(ExpensesDetail editedExpensesDetail) {
        ExpensesDetailDao expensesDetailDao = new ExpensesDetailDao();
        return expensesDetailDao.update(editedExpensesDetail);
    }

    public int delete(ExpensesDetail editedExpensesDetail) {
        ExpensesDetailDao expensesDetailDao = new ExpensesDetailDao();
        return expensesDetailDao.delete(editedExpensesDetail);
    }
}
