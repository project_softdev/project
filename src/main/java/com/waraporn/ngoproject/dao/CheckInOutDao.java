/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.dao;

import com.waraporn.ngoproject.databasehelper.DatabaseHelper;
import com.waraporn.ngoproject.model.CheckInout;
import com.waraporn.ngoproject.model.CheckInout;
import com.waraporn.ngoproject.model.CheckInout;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class CheckInOutDao implements Dao<CheckInout> {

    @Override
    public CheckInout get(int id) {
        CheckInout checkInout = null;
        String sql = "SELECT * FROM check_in_out WHERE check_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkInout = CheckInout.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkInout;
    }

    public CheckInout getDate(String date) {
        CheckInout checkInout = null;
        String sql = "SELECT * FROM check_in_out WHERE date = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, date);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkInout = CheckInout.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkInout;
    }

    @Override
    public List<CheckInout> getAll() {
        ArrayList<CheckInout> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInout checkInout = CheckInout.fromRS(rs);
                list.add(checkInout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckInout> getAll(String where, String order) {
        ArrayList<CheckInout> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInout checkInout = CheckInout.fromRS(rs);
                list.add(checkInout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckInout> getAll(String order) {
        ArrayList<CheckInout> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInout checkInout = CheckInout.fromRS(rs);
                list.add(checkInout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckInout save(CheckInout obj) {

        String sql = "INSERT INTO check_in_out (date,check_in,check_out,total_hour,emp_id)"
                + "VALUES(?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getDate());
            stmt.setString(2, obj.getCheckIn());
            stmt.setString(3, obj.getCheckOut());
            stmt.setInt(4, obj.getTotalHour());
            stmt.setInt(5, obj.getEmpId());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckInout update(CheckInout obj) {
        String sql = "UPDATE check_in_out"
                + " SET date = ? , check_in = ? ,"
                + "check_out = ? , total_hour = ?,emp_id = ?"
                + " WHERE check_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getDate());
            stmt.setString(2, obj.getCheckIn());
            stmt.setString(3, obj.getCheckOut());
            stmt.setInt(4, obj.getTotalHour());
            stmt.setInt(5, obj.getEmpId());
            stmt.setInt(6, obj.getId());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckInout obj) {
        String sql = "DELETE FROM check_in_out WHERE check_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<CheckInout> getByEmployeeId(int id, String where) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
