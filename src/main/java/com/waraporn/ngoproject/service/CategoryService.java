/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.CategoryDao;
import com.waraporn.ngoproject.model.Category;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class CategoryService {
    
    
    public Category getById(int id) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.get(id);
    }
    
    public ArrayList<Category> getByName() {
        CategoryDao categoryDao = new CategoryDao();
        return (ArrayList<Category>) categoryDao.getAll(" category_name asc");
    }
    
    public Category getName(String name) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.get(name);
    }
    
    public List<Category> getCategorys(){
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.getAll(" category_id asc");
    }

    public Category addNew(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.save(editedCategory);
    }

    public Category update(Category editedCategory) {
         if (editedCategory != null) {
            CategoryDao proDao = new CategoryDao();
            return proDao.update(editedCategory);
        } else {
            System.err.println("Cannot update a null category.");
            return null;
        }
    }

    public int delete(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.delete(editedCategory);
    }
}
