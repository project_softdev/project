/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import com.waraporn.ngoproject.dao.ProductDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class RecieptDetail {

    private int Id;
    private int productId;
    private String productName;
    private float productPrice;
    private int qty;
    private float totalPrice;
    private int recieptId;
    private Reciept reciept;
    private Product product;
    

    public RecieptDetail(int id, String productName,float productPrice, int productId, int qty, float totalPrice, int recieptId) {
        this.Id = id;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
        
    }

    public RecieptDetail(int productId, String productName,float productPrice, int qty, float totalPrice, int recieptId) {
        this.Id = -1;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
        
    }
    
//    public RecieptDetail(String productName, float productPrice, int qty, float totalPrice) {
//        this.Id = -1;
//        this.productId = 0;
//        this.productName = productName;
//        this.productPrice = productPrice;
//        this.qty = qty;
//        this.totalPrice = totalPrice;
//        this.recieptId = 0;
//    }
//    
//    public RecieptDetail(String productName, int qty) {
//        this.Id = -1;
//        this.productId = 0;
//        this.productName = productName;
//        this.productPrice = 0;
//        this.qty = qty;
//        this.totalPrice = 0;
//        this.recieptId = 0;
//    }

    public RecieptDetail() {
        this.Id = -1;
        this.productId = 0;
        this.productName = "";
        this.productPrice = 0;
        this.qty = 0;
        this.totalPrice = 0;
        this.recieptId = 0;
       
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        totalPrice = qty*productPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getRecieptId() {
        return recieptId;
    }

    public void setRecieptId(int recieptId) {
        this.recieptId = recieptId;
    }

    public Reciept getReciept() {
        return reciept;
    }

    public void setReciept(Reciept reciept) {
        this.reciept = reciept;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "RecieptDetail{" + "Id=" + Id + ", productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice + ", qty=" + qty + ", totalPrice=" + totalPrice + ", recieptId=" + recieptId + ", reciept=" + reciept + ", product=" + product  + '}';
    }
    

    


    public static RecieptDetail fromRS(ResultSet rs) {
        RecieptDetail recieptDetail = new RecieptDetail();
        try {
            recieptDetail.setId(rs.getInt("reciept_detail_id"));
            recieptDetail.setProductId(rs.getInt("product_id"));
            recieptDetail.setProductName(rs.getString("product_name"));
            recieptDetail.setProductPrice(rs.getFloat("product_price"));
            recieptDetail.setQty(rs.getInt("qty"));
            recieptDetail.setTotalPrice(rs.getFloat("total_price"));
            recieptDetail.setRecieptId(rs.getInt("reciept_id"));
            
            ProductDao productDao = new ProductDao();
            Product product = productDao.get(rs.getInt("product_id"));
            recieptDetail.setProduct(product);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(RecieptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetail;
    }

}
