/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.RecieptDao;
import com.waraporn.ngoproject.dao.RecieptDetailDao;
import com.waraporn.ngoproject.model.Reciept;
import com.waraporn.ngoproject.model.RecieptDetail;
import java.util.List;

/**
 *
 * @author ACER
 */
public class RecieptService {

    public Reciept getById(int id) {
        RecieptDao recieptDao = new RecieptDao();
        Reciept reciept = recieptDao.get(id);
        return reciept;
    }

    public List<Reciept> getReciepts() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getAll(" reciept_id asc");
    }

    public Reciept addNew(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        Reciept reciept =  recieptDao.save(editedReciept);
        for(RecieptDetail rd: editedReciept.getRecieptDetails()){
             rd.setRecieptId(reciept.getId());
            recieptDetailDao.save(rd);
        }
        return reciept;
    }
    
    public Reciept update(Reciept editedReciept) {
        if(editedReciept != null){
            RecieptDao recieptDao = new RecieptDao();
            return recieptDao.update(editedReciept);
        }else {
            System.out.println("Cannot update a null bill.");
            return null;
        }
    }

    public int delete(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.delete(editedReciept);
    }
}
