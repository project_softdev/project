/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class Member {

    private int id;
    private String memberName;
    private String memberTel;
    private int memberPoint;
    private int customerId;
    private Customer customer;

    public Member(int id, String memberName, String memberTel, int memberPoint, int customerId) {
        this.id = id;
        this.memberName = memberName;
        this.memberTel = memberTel;
        this.memberPoint = memberPoint;
        this.customerId = customerId;
    }

    public Member(String memberName, String memberTel, int memberPoint, int customerId) {
        this.id = -1;
        this.memberName = memberName;
        this.memberTel = memberTel;
        this.memberPoint = memberPoint;
        this.customerId = customerId;
    }
    
    public Member(String memberName, String memberTel, int memberPoint) {
        this.id = -1;
        this.memberName = memberName;
        this.memberTel = memberTel;
        this.memberPoint = memberPoint;
        this.customerId = -1;
    }

    public Member() {
        this.id = -1;
        this.memberName = "";
        this.memberTel = "";
        this.memberPoint = 0;
        this.customerId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberTel() {
        return memberTel;
    }

    public void setMemberTel(String memberTel) {
        this.memberTel = memberTel;
    }

    public int getMemberPoint() {
        return memberPoint;
    }

    public void setMemberPoint(int memberPoint) {
        this.memberPoint = memberPoint;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.customerId = customer.getId();
    }

    @Override
    public String toString() {
        return "Member{" + "id=" + id + ", memberName=" + memberName + ", memberTel=" + memberTel + ", memberPoint=" + memberPoint + ", customerId=" + customerId + '}';
    }

    public static Member fromRS(ResultSet rs) {
        Member member = new Member();
        try {
            member.setId(rs.getInt("member_id"));
            member.setMemberName(rs.getString("member_name"));
            member.setMemberTel(rs.getString("member_tel"));
            member.setMemberPoint(rs.getInt("member_point"));
            member.setCustomerId(rs.getInt("customer_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return member;
    }

}
