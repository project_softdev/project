/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_Service;

import com.waraporn.ngoproject.model.Material;
import com.waraporn.ngoproject.service.MaterialService;

/**
 *
 * @author DELL
 */
public class TestMaterial {
    public static void main(String[] args) {
        MaterialService ms = new MaterialService();
        Material mt = new Material("doe", 5, 0, "owef", 10, "หมด");
        ms.addNew(mt);
        System.out.println("add new");
        for(Material material : ms.getMaterials()){
            System.out.println(material);
        }
        
        // update , delete customer
        Material delCus = ms.getById(12);
        delCus.setName("มะนาว");
        ms.update(delCus);
        System.out.println("After Updated");
        for(Material material : ms.getMaterials()){
            System.out.println(material);
        }
    }
}
