/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.CheckInOutDao;
import com.waraporn.ngoproject.model.CheckInout;
import java.util.List;

/**
 *
 * @author ACER
 */
public class CheckInOutService {
    public List<CheckInout> getByEmployeeId(int id,String where) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getByEmployeeId(id,where);
    }

    public List<CheckInout> getCheckInouts() {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getAll(" check_id asc");
    }

    public CheckInout addNew(CheckInout editedCheckInout) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.save(editedCheckInout);
    }

    public CheckInout update(CheckInout editedCheckInout) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.update(editedCheckInout);
    }

    public int delete(CheckInout editedCheckInout) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.delete(editedCheckInout);
    }
}
