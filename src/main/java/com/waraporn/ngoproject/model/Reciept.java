/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import com.waraporn.ngoproject.dao.CustomerDao;
import com.waraporn.ngoproject.dao.MemberDao;
import com.waraporn.ngoproject.dao.RecieptDetailDao;
import com.waraporn.ngoproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class Reciept {

    private int id;
    private Date createdDate;
    private float total;
    private float cash;
    private float change;
    private float discount;
    private int totalQty;
    private int userId;
    private int customerId;
    private int memberId;
    private User user;
    private Customer customer;
    private Member member;
    private ArrayList<RecieptDetail> recieptDetails = new ArrayList<RecieptDetail>();
    private String paymentType;

    public Reciept(int id, Date createdDate, float total, float cash, float change, float discount, int totalQty, int userId, int customerId, int memberId, String paymentType) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.change = change;
        this.discount = discount;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
        this.memberId = memberId;
        this.paymentType = paymentType;
    }

    public Reciept(Date createdDate, float total, float cash, float change, float discount, int totalQty, int userId, int customerId, int memberId, String paymentType) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.change = change;
        this.discount = discount;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
        this.memberId = memberId;
        this.paymentType = paymentType;
    }

    public Reciept(float total, float cash, float change, float discount, int totalQty, int userId, int customerId, int memberId, String paymentType) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.cash = cash;
        this.change = change;
        this.discount = discount;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
        this.memberId = memberId;
        this.paymentType = paymentType;
    }

    public Reciept(float total, float cash, float change, float discount, int totalQty, String paymentType) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.cash = cash;
        this.change = change;
        this.discount = discount;
        this.totalQty = totalQty;
        this.userId = 0;
        this.customerId = 0;
        this.memberId = 0;
        this.paymentType = "";
    }

    public Reciept() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = 0;
        this.change = 0;
        this.discount = 0;
        this.totalQty = 0;
        this.userId = 0;
        this.customerId = 0;
        this.memberId = 0;
        this.paymentType = "";
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int empId) {
        this.userId = empId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public ArrayList<RecieptDetail> getRecieptDetails() {
        return recieptDetails;
    }

    public void setRecieptDetails(ArrayList<RecieptDetail> recieptDetails) {
        this.recieptDetails = recieptDetails;
    }

    public void addRecieptDatails(RecieptDetail recieptDetail) {
        
        recieptDetails.add(recieptDetail);
        calculateTotal();
    }

    public void addRecieptDatails(Product product,int qty) {
        RecieptDetail rd = new RecieptDetail(product.getId(),product.getName(),
                product.getPrice(),qty,product.getPrice()*qty,-1);
        recieptDetails.add(rd);
        calculateTotal();
    }
//    public void addRecieptDatails(Product product, String productName, float price, int qty) {
//        RecieptDetail recieptDetail = new RecieptDetail(-1, product.getName(), product.getPrice(), qty, product.getPrice() * qty, -1);
//        recieptDetails.add(recieptDetail);
//        calculateTotal();
//    }
    public void delRecieptDatails(RecieptDetail recieptDetail) {
        this.recieptDetails.remove(recieptDetail);
        calculateTotal();
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", cash=" + cash + ", change=" + change + ", discount=" + discount + ", totalQty=" + totalQty + ", userId=" + userId + ", customerId=" + customerId + ", memberId=" + memberId + ", user=" + user + ", customer=" + customer + ", member=" + member + ", recieptDetails=" + recieptDetails + ", paymentType=" + paymentType + '}';
    }

   

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (RecieptDetail rd : recieptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.totalQty = totalQty;
        this.total = total;
    }

    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("reciept_id"));
            reciept.setCreatedDate(rs.getTimestamp("created_date"));
            reciept.setTotal(rs.getFloat("total"));
            reciept.setCash(rs.getFloat("cash"));
            reciept.setCash(rs.getFloat("change"));
            reciept.setCash(rs.getFloat("discount"));
            reciept.setTotalQty(rs.getInt("total_qty"));
            reciept.setUserId(rs.getInt("user_id"));
            reciept.setCustomerId(rs.getInt("customer_id"));
            reciept.setMemberId(rs.getInt("member_id"));
            reciept.setPaymentType(rs.getString("payment_type"));
            //Population

            UserDao userDao = new UserDao();
            User user = userDao.get(reciept.getUserId());
            reciept.setUser(user);

            CustomerDao customerDao = new CustomerDao();
            Customer customer = customerDao.get(reciept.getCustomerId());
            if(customer!= null){
               reciept.setCustomer(customer);
            }           

            MemberDao MemberDao = new MemberDao();
            Member member = MemberDao.get(reciept.getMemberId());            
            if(member!= null){
               reciept.setMember(member); 
            }
            
            RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
            reciept.setRecieptDetails((ArrayList<RecieptDetail>)recieptDetailDao.getByCheckRecieptId(reciept.getId()));
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
}
