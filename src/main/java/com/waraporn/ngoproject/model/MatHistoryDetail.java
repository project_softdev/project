/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class MatHistoryDetail {
    private int id;
    private String matName;
    private int oldQty;
    private int newQty;
    private int usedQty;
    private String unit;
    private int cmhId;
    private int matId;
    private MatHistory matHistory;

    public MatHistoryDetail(int id, String matName, int oldQty, int newQty, int usedQty, String unit, int cmhId, int matId) {
        this.id = id;
        this.matName = matName;
        this.oldQty = oldQty;
        this.newQty = newQty;
        this.usedQty = usedQty;
        this.unit = unit;
        this.cmhId = cmhId;
        this.matId = matId;
    }

    public MatHistoryDetail(String matName, int oldQty, int newQty, int usedQty, String unit, int cmhId, int matId) {
        this.id = -1;
        this.matName = matName;
        this.oldQty = oldQty;
        this.newQty = newQty;
        this.usedQty = usedQty;
        this.unit = unit;
        this.cmhId = cmhId;
        this.matId = matId;
    }
    
    public MatHistoryDetail(String matName, int oldQty, int newQty, int usedQty, String unit) {
        this.id = -1;
        this.matName = matName;
        this.oldQty = oldQty;
        this.newQty = newQty;
        this.usedQty = usedQty;
        this.unit = unit;
        this.cmhId = 0;
        this.matId = 0;
    }
    
    public MatHistoryDetail() {
        this.id = -1;
        this.matName = "";
        this.oldQty = 0;
        this.newQty = 0;
        this.usedQty = 0;
        this.unit = "";
        this.cmhId = 0;
        this.matId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMatName() {
        return matName;
    }

    public void setMatName(String matName) {
        this.matName = matName;
    }

    public int getOldQty() {
        return oldQty;
    }

    public void setOldQty(int oldQty) {
        this.oldQty = oldQty;
    }

    public int getNewQty() {
        return newQty;
    }

    public void setNewQty(int newQty) {
        this.newQty = newQty;
    }

    public int getUsedQty() {
        return usedQty;
    }

    public void setUsedQty(int usedQty) {
        this.usedQty = usedQty;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getCmhId() {
        return cmhId;
    }

    public void setCmhId(int cmhId) {
        this.cmhId = cmhId;
    }

    public int getMatId() {
        return matId;
    }

    public void setMatId(int matId) {
        this.matId = matId;
    }

    @Override
    public String toString() {
        return "MatHistoryDetail{" + "id=" + id + ", matName=" + matName + ", oldQty=" + oldQty + ", newQty=" + newQty + ", usedQty=" + usedQty + ", unit=" + unit + ", cmhId=" + cmhId + ", matId=" + matId + ", matHistory=" + matHistory + '}';
    }
    
     
    public static MatHistoryDetail fromRS(ResultSet rs) {
        MatHistoryDetail cmhDetail = new MatHistoryDetail();
        try {
            cmhDetail.setId(rs.getInt("cmh_detail_id"));
            cmhDetail.setMatName(rs.getString("mat_name"));
            cmhDetail.setOldQty(rs.getInt("old_qty"));
            cmhDetail.setNewQty(rs.getInt("new_qty"));
            cmhDetail.setUsedQty(rs.getInt("used_qty"));
            cmhDetail.setUnit(rs.getString("unit"));
            cmhDetail.setCmhId(rs.getInt("cmh_id"));
            cmhDetail.setMatId(rs.getInt("mat_id"));
            
        } catch (SQLException ex) {
            Logger.getLogger(MatHistory.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return cmhDetail;
    }
}
