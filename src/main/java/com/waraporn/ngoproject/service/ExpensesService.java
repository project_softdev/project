/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.ExpensesDao;
import com.waraporn.ngoproject.dao.ExpensesDetailDao;
import com.waraporn.ngoproject.model.Expenses;
import com.waraporn.ngoproject.model.ExpensesDetail;
import com.waraporn.ngoproject.report.ExpensesReport;
import java.util.List;

/**
 *
 * @author DELL
 */
public class ExpensesService {
    public Expenses getById(int id) {
        ExpensesDao expensesDao = new ExpensesDao();
        Expenses expenses = expensesDao.get(id);
        return expenses;
    }

    public List<Expenses> getExpensess() {
        ExpensesDao expensesDao = new ExpensesDao();
        return expensesDao.getAll(" expenses_id asc");
    }

    public Expenses addNew(Expenses editedExpenses) {
        ExpensesDao expensesDao = new ExpensesDao();
        ExpensesDetailDao expensesDetailDao = new ExpensesDetailDao();
        Expenses expenses = expensesDao.save(editedExpenses);
        for(ExpensesDetail exp : editedExpenses.getExpensesDetails()){
            exp.setExpensesId(expenses.getId());
            expensesDetailDao.save(exp);
        }
        return expenses;
    }

    public Expenses update(Expenses editedExpenses) {
        if(editedExpenses != null){
            ExpensesDao expensesDao = new ExpensesDao();
            return expensesDao.update(editedExpenses);
        }else {
            System.out.println("Cannot update a null expenses.");
            return null;
        }
    }

    public int delete(Expenses editedExpenses) {
        ExpensesDao expensesDao = new ExpensesDao();
        return expensesDao.delete(editedExpenses);
    }
    
    public List<ExpensesReport> getTopTenExpenses() {
        ExpensesDao expensesDao = new ExpensesDao();
        return expensesDao.getExpensesPerMonth();
    }

    public List<ExpensesReport> getTopTenExpenses(String begin, String end) {
        ExpensesDao expensesDao = new ExpensesDao();
        return expensesDao.getExpensesPerMonth(begin, end);
    }
}
