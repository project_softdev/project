/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.waraporn.ngoproject.component;

import com.waraporn.ngoproject.model.Product;

/**
 *
 * @author ACER
 */
public interface BuyProductable {
    public void buy(Product product, int qty);
}
