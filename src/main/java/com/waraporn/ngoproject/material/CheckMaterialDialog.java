/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.waraporn.ngoproject.material;


import com.waraporn.ngoproject.model.MatHistory;
import com.waraporn.ngoproject.model.MatHistoryDetail;
import com.waraporn.ngoproject.model.Material;
import com.waraporn.ngoproject.service.EmployeeService;
import com.waraporn.ngoproject.service.MatHistoryDetailService;
import com.waraporn.ngoproject.service.MatHistoryService;
import com.waraporn.ngoproject.service.MaterialService;
import com.waraporn.ngoproject.service.UserService;
import java.awt.Font;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author DELL
 */
public class CheckMaterialDialog extends javax.swing.JDialog {

    private MaterialService materialService;
    private MatHistoryService matHistoryService = new MatHistoryService();
    private Material editedMaterial;
    private MatHistory editedMatHistory;
    private List<Material> list;
    private EmployeeService empService = new EmployeeService();
    private List<Integer> newQty;
    

    /**
     * Creates new form CheckMaterialDialog
     */
    public CheckMaterialDialog(java.awt.Frame parent, Material editedMaterial) {
        super(parent, true);
        initComponents();
        MatHistory matHistory = new MatHistory();
        matHistory.setUser(UserService.currentUser);
        this.editedMaterial = editedMaterial;
        materialService = new MaterialService();
        
        list = materialService.getMaterials();
        initCheckMatTable();
        newQty = new ArrayList<>();
        for (Material mat : list) {
            newQty.add(0);
        }
    }

    private void initCheckMatTable() {
        tblCheckMat.setRowHeight(100);
        tblCheckMat.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 20));
        tblCheckMat.setModel(new AbstractTableModel() {
            String[] columnNames = {"รหัส", "รูปภาพ", "ชื่อ", "จำนวนเดิม", "จำนวนคงเหลือ", "หน่วย"};
            
            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }
            
            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }
            
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 1:
                        return ImageIcon.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material material = list.get(rowIndex);
                MatHistoryDetailService matHisDetailService = new MatHistoryDetailService();
                MatHistoryDetail matHisDetail = matHisDetailService.getById(material.getId());
                switch (columnIndex) {
                    case 0:
                        return material.getId();
                    case 1:
                        ImageIcon icon = new ImageIcon("./mat" + material.getId() + ".png");
                        Image image = icon.getImage();
                        int width = image.getWidth(null);
                        int height = image.getHeight(null);
                        Image newImage = image.getScaledInstance((int) (100*(float)width/height), 100, Image.SCALE_SMOOTH);
                        icon.setImage(newImage);
                        return icon;  
                    case 2:
                        return material.getName();
                    case 3:
                        return material.getQty();
                    case 4:
                        return newQty.get(rowIndex);
                    case 5:
                        return material.getPrice();
                    default:
                        return "Unknown";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                try {
                    int newValue = Integer.parseInt(aValue.toString());
                    int currentRemain = list.get(rowIndex).getQty();
                    if (newValue > currentRemain) {
                        JOptionPane.showMessageDialog(tblCheckMat, "จำนวนปัจจุบันต้องไม่มากกว่าจำนวนคงเหลือ");
                    } else {
                        newQty.set(rowIndex, newValue);
                        fireTableDataChanged();
                    }
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(tblCheckMat, "กรุณากรอกเฉพาะตัวเลข");

                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch(columnIndex) {
                    case 4:
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        หน้าสินค้า = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCheckMat = new javax.swing.JTable();
        jButton4 = new javax.swing.JButton();
        btnSaveCheckMat = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(224, 201, 171));

        jPanel2.setBackground(new java.awt.Color(224, 201, 171));

        หน้าสินค้า.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        หน้าสินค้า.setText("อัพเดทวัตถุดิบ");

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });

        jButton1.setBackground(new java.awt.Color(102, 51, 0));
        jButton1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("ค้นหา");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(667, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(หน้าสินค้า, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(หน้าสินค้า)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addContainerGap())
        );

        tblCheckMat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblCheckMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblCheckMat);

        jButton4.setBackground(new java.awt.Color(102, 51, 0));
        jButton4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jButton4.setForeground(new java.awt.Color(255, 255, 255));
        jButton4.setText("ยกเลิก");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        btnSaveCheckMat.setBackground(new java.awt.Color(102, 51, 0));
        btnSaveCheckMat.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnSaveCheckMat.setForeground(new java.awt.Color(255, 255, 255));
        btnSaveCheckMat.setText("ตกลง");
        btnSaveCheckMat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveCheckMatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 998, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSaveCheckMat)))
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 465, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSaveCheckMat)
                    .addComponent(jButton4))
                .addGap(15, 15, 15))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveCheckMatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveCheckMatActionPerformed
        MatHistory matHis = new MatHistory();
        UserService userService = new UserService();
        matHis.setUserId(UserService.currentUser.getId());
        int i = 0;
        for (Material mat : list) {
            MatHistoryDetail matHisDetail = new MatHistoryDetail();
            matHisDetail.setMatId(mat.getId());
            matHisDetail.setMatName(mat.getName());
            matHisDetail.setOldQty(mat.getQty());
            matHisDetail.setNewQty(newQty.get(i));
            matHisDetail.setUsedQty(matHisDetail.getOldQty()-matHisDetail.getNewQty());
            matHisDetail.setUnit(mat.getUnit());
            matHis.addMatHistoryDetail(matHisDetail);
            i++;
        }
        matHistoryService.addNew(matHis);
        dispose();
    }//GEN-LAST:event_btnSaveCheckMatActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        TableRowSorter<AbstractTableModel> sorter = new TableRowSorter<>((AbstractTableModel) tblCheckMat.getModel());
        tblCheckMat.setRowSorter(sorter);
        sorter.setRowFilter(RowFilter.regexFilter(txtSearch.getText()));
    }//GEN-LAST:event_txtSearchActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        TableRowSorter<AbstractTableModel> sorter = new TableRowSorter<>((AbstractTableModel) tblCheckMat.getModel());
        tblCheckMat.setRowSorter(sorter);
        sorter.setRowFilter(RowFilter.regexFilter(txtSearch.getText()));
    }//GEN-LAST:event_jButton1ActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSaveCheckMat;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblCheckMat;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JLabel หน้าสินค้า;
    // End of variables declaration//GEN-END:variables
}
