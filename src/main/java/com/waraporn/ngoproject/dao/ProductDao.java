/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.dao;

import com.waraporn.ngoproject.databasehelper.DatabaseHelper;
import com.waraporn.ngoproject.model.Product;
import com.waraporn.ngoproject.report.ProductReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class ProductDao implements Dao<Product>{
    @Override
    public Product get(int id) {
        Product product = null;
        String sql = "SELECT * FROM product WHERE product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                product = Product.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return product;
    }


    public List<Product> getAll() {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Product> getAll(String where, String order) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Product> getAll(String order) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Product save(Product obj) {

        String sql = "INSERT INTO product (product_name, product_price, product_size, product_sweet_level, product_type, category_id)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getPrice());
            stmt.setString(3, obj.getSize());
            stmt.setString(4, obj.getSweetLevel());
            stmt.setString(5, obj.getType());
            stmt.setInt(6, obj.getCategoryId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }        
        return obj;
    }

    @Override
    public Product update(Product obj) {
        String sql = "UPDATE product"
                + " SET product_name = ?, product_price = ?, product_size = ?, product_sweet_level = ?, product_type = ?, category_id = ?"
                + " WHERE product_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getPrice());
            stmt.setString(3, obj.getSize());
            stmt.setString(4, obj.getSweetLevel());
            stmt.setString(5, obj.getType());
            stmt.setInt(6, obj.getCategoryId());
            stmt.setInt(7, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Product obj) {
        String sql = "DELETE FROM product WHERE product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    
    public List<ProductReport> getProductTopSeller(int limit) {
        ArrayList<ProductReport> list = new ArrayList();
        String sql = """
                     SELECT  pro.product_id,
                             pro.product_name,
                             SUM(recd.qty) AS TotalQuantity,
                             SUM(pro.product_price * recd.qty) AS TotalPrice
                        FROM Reciept rec
                             INNER JOIN
                             reciept_detail recd ON rec.reciept_id = recd.reciept_id
                             INNER JOIN
                             product pro ON recd.product_id = pro.product_id
                       GROUP BY pro.product_id,
                                pro.product_name
                       ORDER BY TotalPrice DESC
                       LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProductReport productReport = ProductReport.fromRS(rs);
                list.add(productReport);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ProductReport> getProductTopSeller(String begin, String end, int limit) {
        ArrayList<ProductReport> list = new ArrayList();
        String sql = """
                     SELECT  pro.product_id,
                             pro.product_name,
                             SUM(recd.qty) AS TotalQuantity,
                             SUM(pro.product_price * recd.qty) AS TotalPrice
                        FROM Reciept rec
                             INNER JOIN reciept_detail recd ON rec.reciept_id = recd.reciept_id
                             INNER JOIN product pro ON recd.product_id = pro.product_id
                             AND rec.created_date BETWEEN ? AND ?
                       GROUP BY pro.product_id,
                                pro.product_name
                       ORDER BY TotalPrice DESC
                       LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProductReport productReport = ProductReport.fromRS(rs);
                list.add(productReport);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
