/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.waraporn.ngoproject.pos;

import com.waraporn.ngoproject.component.BuyProductable;
import com.waraporn.ngoproject.component.ProductListPanel;
import com.waraporn.ngoproject.member.AddMemberDialog;
import com.waraporn.ngoproject.model.Member;
import com.waraporn.ngoproject.model.Product;
import com.waraporn.ngoproject.model.Reciept;
import com.waraporn.ngoproject.model.RecieptDetail;
import com.waraporn.ngoproject.service.MemberService;
import com.waraporn.ngoproject.service.ProductService;
import com.waraporn.ngoproject.service.RecieptService;
import com.waraporn.ngoproject.service.UserService;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author ACER
 */
public class POSpanel extends javax.swing.JPanel implements BuyProductable {

    ProductService productSevice = new ProductService();
    RecieptService recieptService = new RecieptService();
    Reciept reciept;
    private final ProductListPanel productListPanel;
    private MemberService memberService;
    private List<Member> listMember;
    private Member editedMember;
    Reciept editedReciept;

    /**
     * Creates new form POSpanel
     */
    public POSpanel() {
        initComponents();
        productListPanel = new ProductListPanel();
        scrProductList.setViewportView(productListPanel);
        reciept = new Reciept();
        reciept.setUser(UserService.currentUser);
        productListPanel.addOnByProduct(this);

        productSevice.getByName();
        tblReciepDetail.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 18));
        tblReciepDetail.setRowHeight(40);
        tblReciepDetail.setModel(new AbstractTableModel() {
            String[] headers = {"รายการ", "ราคา", "จำนวน", "รวม"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return reciept.getRecieptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetail.getProductName();
                    case 1:
                        return recieptDetail.getProductPrice();
                    case 2:
                        return recieptDetail.getQty();
                    case 3:
                        return recieptDetail.getTotalPrice();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    recieptDetail.setQty(qty);
                    reciept.calculateTotal();
                    refreshReciept();

                }

            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }

        });

        TableMemBer();
    }

    private void TableMemBer() {
        memberService = new MemberService();

        listMember = memberService.getMembers();
        tblMember.setRowHeight(30);
        editedMember = new Member();
        tblMember.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 16));
        tblMember.setModel(new AbstractTableModel() {
            String[] header = {"รหัส", "ชื่อ", "เบอร์", "คะแนน"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return listMember.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Member member = listMember.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return member.getId();
                    case 1:
                        return member.getMemberName();
                    case 2:
                        return member.getMemberTel();
                    case 3:
                        return member.getMemberPoint();
                    default:
                        return "";
                }
            }
        });
    }

    private void refreshReciept() {
        tblReciepDetail.revalidate();
        tblReciepDetail.repaint();
        lblTotal.setText("" + reciept.getTotal());

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        lblTotal = new javax.swing.JLabel();
        btnPay = new javax.swing.JButton();
        btnCencel = new javax.swing.JButton();
        lblText = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblMember = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblReciepDetail = new javax.swing.JTable();
        btnAddMember = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        scrProductList = new javax.swing.JScrollPane();

        jPanel3.setBackground(new java.awt.Color(248, 247, 230));

        lblTotal.setFont(new java.awt.Font("Segoe UI", 1, 20)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(125, 108, 83));
        lblTotal.setText("0.00");

        btnPay.setBackground(new java.awt.Color(153, 255, 153));
        btnPay.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        btnPay.setForeground(new java.awt.Color(125, 108, 83));
        btnPay.setText("Payment");
        btnPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPayActionPerformed(evt);
            }
        });

        btnCencel.setBackground(new java.awt.Color(255, 204, 204));
        btnCencel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCencel.setForeground(new java.awt.Color(80, 69, 69));
        btnCencel.setText("ยกเลิกออร์เดอร์");
        btnCencel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCencelActionPerformed(evt);
            }
        });

        lblText.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblText.setForeground(new java.awt.Color(125, 108, 83));
        lblText.setText("ราคารวม :");

        tblMember.setBackground(new java.awt.Color(255, 255, 204));
        tblMember.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblMember.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblMember);

        tblReciepDetail.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblReciepDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblReciepDetail);

        btnAddMember.setBackground(new java.awt.Color(255, 255, 204));
        btnAddMember.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnAddMember.setForeground(new java.awt.Color(103, 79, 79));
        btnAddMember.setText("สมัครสมาชิก");
        btnAddMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddMemberActionPerformed(evt);
            }
        });

        btnSearch.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(103, 79, 79));
        btnSearch.setText("ค้นหา");
        btnSearch.setToolTipText("");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(lblText)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTotal))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(txtSearch)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSearch))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAddMember, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCencel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPay)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblText)
                    .addComponent(lblTotal))
                .addGap(12, 12, 12)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnPay, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCencel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSearch))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAddMember, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        scrProductList.setBackground(new java.awt.Color(229, 217, 201));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrProductList, javax.swing.GroupLayout.DEFAULT_SIZE, 813, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrProductList))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPayActionPerformed
        //System.out.println("" + reciept);
        //recieptService.addNew(reciept);
        OpenDialogPayment();
        clearReciept();

    }//GEN-LAST:event_btnPayActionPerformed

    private void btnCencelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCencelActionPerformed
        clearReciept();
    }//GEN-LAST:event_btnCencelActionPerformed

    private void btnAddMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddMemberActionPerformed
        editedMember = new Member();
        OpenDialogAddMember();
    }//GEN-LAST:event_btnAddMemberActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        TableRowSorter<AbstractTableModel> sorter = new TableRowSorter<>((AbstractTableModel) tblMember.getModel());
        tblMember.setRowSorter(sorter);
        sorter.setRowFilter(RowFilter.regexFilter(txtSearch.getText()));
    }//GEN-LAST:event_btnSearchActionPerformed

    private void clearReciept() {
        reciept = new Reciept();
        reciept.setUser(UserService.currentUser);
        refreshReciept();
    }

    private void OpenDialogAddMember() {
        JFrame jframe = (JFrame) SwingUtilities.getRoot(this);
        AddMemberDialog memberDialog = new AddMemberDialog(jframe, editedMember);
        memberDialog.setLocationRelativeTo(this);
        memberDialog.setVisible(true);
        memberDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }

        });

    }

    private void OpenDialogPayment() {
        JFrame jframe = (JFrame) SwingUtilities.getRoot(this);
        PaymentDialog paymentDialog = new PaymentDialog(jframe, reciept);
        paymentDialog.setLocationRelativeTo(this);
        paymentDialog.setVisible(true);
        paymentDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }

        });

    }

    private void refreshTable() {
        listMember = memberService.getMembers();
        tblMember.revalidate();
        tblMember.repaint();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddMember;
    private javax.swing.JButton btnCencel;
    private javax.swing.JButton btnPay;
    private javax.swing.JButton btnSearch;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblText;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JScrollPane scrProductList;
    private javax.swing.JTable tblMember;
    private javax.swing.JTable tblReciepDetail;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int qty) {
        System.out.println(product + " " + qty);

        reciept.addRecieptDatails(product, qty);

//        if (reciept.getTotal() >= 700) {
//
//            if (reciept.getTotal() >= 100) {
//
//                Float total = reciept.getTotal();
//                Float result = (total * 5) / 100;
//                Float net = total - result;
//                System.out.println(total);
//
//
//            }
//           /refreshReciept();

        
        refreshReciept();

    }
}
