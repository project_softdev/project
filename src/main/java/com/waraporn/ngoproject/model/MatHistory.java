/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import com.waraporn.ngoproject.dao.MatHistoryDetailDao;
import com.waraporn.ngoproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class MatHistory {
    private int id;
    private Timestamp date;
    private int userId;
    private User user;
    private ArrayList<MatHistoryDetail> matHistoryDetails = new ArrayList();

    public MatHistory(int id, Timestamp date, int userId) {
        this.id = id;
        this.date = date;
        this.userId = userId;
    }
    
    public MatHistory(Timestamp date, int userId) {
        this.id = -1;
        this.date = date;
        this.userId = userId;
    }
    
    public MatHistory(int userId) {
        this.id = -1;
        this.date = null;
        this.userId = userId;
    }
    
    public MatHistory() {
        this.id = -1;
        this.date = null;
        this.userId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    

    public ArrayList<MatHistoryDetail> getMatHistoryDetails() {
        return matHistoryDetails;
    }

    public void setMatHistoryDetails(ArrayList matHistoryDetails) {
        this.matHistoryDetails = matHistoryDetails;
    }

    @Override
    public String toString() {
        return "MatHistory{" + "id=" + id + ", date=" + date + ", userId=" + userId + ", user=" + user + ", matHistoryDetails=" + matHistoryDetails + '}';
    }
  
    public void addMatHistoryDetail(MatHistoryDetail matHistoryDetail) {
        matHistoryDetails.add(matHistoryDetail);
    }

    
    public void delMatHistoryDetail() {
        matHistoryDetails.clear();
    }

    
    public static MatHistory fromRS(ResultSet rs) {
        MatHistory cmh = new MatHistory();
        try {
            cmh.setId(rs.getInt("cmh_id"));
            cmh.setDate(rs.getTimestamp("cmh_date"));
            cmh.setUserId(rs.getInt("user_id"));
            
            MatHistoryDetailDao matHisDetailDao = new MatHistoryDetailDao();
            cmh.setMatHistoryDetails((ArrayList<MatHistoryDetail>) matHisDetailDao.getByCheckMatHistoryId(cmh.getId()));
            
            UserDao userDao = new UserDao();
            User user = userDao.get(cmh.getId());
            cmh.setUser(user);
            
        } catch (SQLException ex) {
            Logger.getLogger(MatHistory.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return cmh;
    }
    
}
