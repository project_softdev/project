/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.MatHistoryDao;
import com.waraporn.ngoproject.dao.MatHistoryDetailDao;
import com.waraporn.ngoproject.model.MatHistory;
import com.waraporn.ngoproject.model.MatHistoryDetail;
import com.waraporn.ngoproject.model.Material;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class MatHistoryService {

    public MatHistory getById(int id) {
        MatHistoryDao cmhDao = new MatHistoryDao();
        MatHistory cmh = cmhDao.get(id);
        return cmh;
    }

    public List<MatHistory> getMatHistorys() {
        MatHistoryDao cmhDao = new MatHistoryDao();
        return cmhDao.getAll(" cmh_id asc");
    }

    public MatHistory addNew(MatHistory editedMatHistory) {
        MaterialService materialService = new MaterialService();
        MatHistoryDao matHisDao = new MatHistoryDao();
        MatHistoryDetailDao matHistoryDetailDao = new MatHistoryDetailDao();
        MatHistory matHis = matHisDao.save(editedMatHistory);
        for (MatHistoryDetail mhd : editedMatHistory.getMatHistoryDetails()) {
            try {
                Material mat = materialService.getId(mhd.getMatId());
                mat.setQty(mhd.getNewQty());
                if (mhd.getNewQty() > 0 && mhd.getNewQty() < mat.getMin()) {
                    mat.setStatus("ใกล้หมด");
                } else if (mhd.getNewQty() == 0) {
                    mat.setStatus("หมด");
                } else {
                    mat.setStatus("คงเหลือ");
                }
                materialService.update(mat);
                mhd.setCmhId(matHis.getId());
                matHistoryDetailDao.save(mhd);
            } catch (Exception ex) {
                Logger.getLogger(MatHistoryService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return matHis;
    }

    public MatHistory update(MatHistory editedMatHistory) {
        if (editedMatHistory != null) {
            MatHistoryDao cmhDao = new MatHistoryDao();
            return cmhDao.update(editedMatHistory);
        } else {
            System.out.println("Cannot update a null matHistory.");
            return null;
        }
    }

    public int delete(MatHistory editedMatHistory) {
        MatHistoryDao cmhDao = new MatHistoryDao();
        return cmhDao.delete(editedMatHistory);
    }
}
