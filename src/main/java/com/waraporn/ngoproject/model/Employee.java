/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import com.waraporn.ngoproject.service.EmployeeService;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author patch
 */
public class Employee {

    private int id;
    private String name;
    private String tel;
    private String email;
    private String position;
    private int hourly_wage;

    public Employee(int id, String name, String tel, String email, String position, int hourly_wage) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.email = email;
        this.position = position;
        this.hourly_wage = hourly_wage;
    }

    public Employee(String name, String tel, String email, String position, int hourly_wage) {
        this.id = -1;
        this.name = name;
        this.tel = tel;
        this.email = email;
        this.position = position;
        this.hourly_wage = hourly_wage;
    }

    public Employee() {
        this.id = -1;
        this.name = "";
        this.tel = "";
        this.email = "";
        this.hourly_wage = 0;
        this.position = "";

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getHourly_wage() {
        return hourly_wage;
    }

    public void setHourly_wage(int hourly_wage) {
        this.hourly_wage = hourly_wage;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", email=" + email + ", position=" + position + ", hourly_wage=" + hourly_wage + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("emp_id"));
            employee.setName(rs.getString("emp_name"));
            employee.setTel(rs.getString("emp_tel"));
            employee.setEmail(rs.getString("emp_email"));
            employee.setPosition(rs.getString("emp_position"));
            employee.setHourly_wage(rs.getInt("emp_hourly_wage"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }

}
