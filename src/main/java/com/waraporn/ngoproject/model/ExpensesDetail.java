/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import com.waraporn.ngoproject.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class ExpensesDetail {
    private int id;
    private String name;
    private float price;
    private int expensesId;
    private Expenses expenses;

    public ExpensesDetail(int id, String name, float price, int expensesId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.expensesId = expensesId;
    }
    
    public ExpensesDetail(String name, float price, int expensesId) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.expensesId = expensesId;
    }
    
    public ExpensesDetail(String name, float price) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.expensesId = 0;
    }
    
    public ExpensesDetail() {
        this.id = -1;
        this.name = "";
        this.price = 0.0f;
        this.expensesId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getExpensesId() {
        return expensesId;
    }

    public void setExpensesId(int expensesId) {
        this.expensesId = expensesId;
    }

    @Override
    public String toString() {
        return "ExpensesDetail{" + "id=" + id + ", name=" + name + ", price=" + price + ", expensesId=" + expensesId + ", expenses=" + expenses + '}';
    }
    
    public static ExpensesDetail fromRS(ResultSet rs) {
        ExpensesDetail expensesDetail = new ExpensesDetail();
        try {
            expensesDetail.setId(rs.getInt("expenses_detail_id"));
            expensesDetail.setName(rs.getString("expenses_detail_name"));
            expensesDetail.setPrice(rs.getFloat("expenses_detail_price"));
            expensesDetail.setExpensesId(rs.getInt("expenses_id"));
        } catch (SQLException ex) {
            Logger.getLogger(ExpensesDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return expensesDetail;
    }
}
