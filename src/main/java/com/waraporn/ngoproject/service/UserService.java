/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.UserDao;
import com.waraporn.ngoproject.model.User;
import java.util.List;

/**
 *
 * @author patch
 */
public class UserService {
    public static User currentUser;
    
    public User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if(user != null && user.getPassword().equals(password)) {
            currentUser = user;
            System.out.println("Success");
            System.out.println(currentUser.getName());
            System.out.println(currentUser);
            return user;
        } else {
            System.out.println("No");
        }
        return null;
    }

//    public static User getCurrentUser() {
//        return new User(1, "pasinee", "F", "password", "พนักงาน", "Pasinee", 1);
//    }
    
    public User getById(int id) {
        UserDao userDao = new UserDao();
        return userDao.get(id);
    }

    public List<User> getUsers(){
        UserDao userDao = new UserDao();
        return userDao.getAll(" user_id asc");
    }
    public User addNew(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.save(editedUser);
    }

    public User update(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.update(editedUser);
    }

    public int delete(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.delete(editedUser);
    }
}
