/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.MatHistoryDetailDao;
import com.waraporn.ngoproject.model.MatHistoryDetail;
import java.util.List;

/**
 *
 * @author DELL
 */
public class MatHistoryDetailService {
    
    public MatHistoryDetail getById(int id) {
        MatHistoryDetailDao cmhDetailDao = new MatHistoryDetailDao();
        MatHistoryDetail cmhDetail = cmhDetailDao.get(id);
        return cmhDetail;
    }

    public List<MatHistoryDetail> getMatHistorysDetail() {
        MatHistoryDetailDao cmhDetailDao = new MatHistoryDetailDao();
        return cmhDetailDao.getAll(" cmh_detail_id asc");
    }

    public MatHistoryDetail addNew(MatHistoryDetail editedMatHistoryDetail) {
        MatHistoryDetailDao cmhDetailDao = new MatHistoryDetailDao();
        return cmhDetailDao.save(editedMatHistoryDetail);
    }

    public MatHistoryDetail update(MatHistoryDetail editedMatHistoryDetail) {
        MatHistoryDetailDao cmhDetailDao = new MatHistoryDetailDao();
        return cmhDetailDao.update(editedMatHistoryDetail);
    }

    public int delete(MatHistoryDetail editedMatHistoryDetail) {
        MatHistoryDetailDao cmhDetailDao = new MatHistoryDetailDao();
        return cmhDetailDao.delete(editedMatHistoryDetail);
    }
    
}
