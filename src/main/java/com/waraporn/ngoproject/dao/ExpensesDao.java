/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.dao;

import com.waraporn.ngoproject.databasehelper.DatabaseHelper;
import com.waraporn.ngoproject.model.Expenses;
import com.waraporn.ngoproject.model.Expenses;
import com.waraporn.ngoproject.model.Expenses;
import com.waraporn.ngoproject.model.ExpensesDetail;
import com.waraporn.ngoproject.report.ExpensesReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class ExpensesDao implements Dao<Expenses>{

    @Override
    public Expenses get(int id) {
        Expenses expenses = null;
        String sql = "SELECT * FROM expenses WHERE expenses_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                expenses = Expenses.fromRS(rs);
                ExpensesDetailDao expd = new ExpensesDetailDao();
                ArrayList<ExpensesDetail> expensesDetails = (ArrayList<ExpensesDetail>)expd.getAll("expenses_id=" + expenses.getId(),"expenses_detail_id");
                expenses.setExpensesDetails(expensesDetails);      
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return expenses;
    }

    @Override
    public List<Expenses> getAll() {
        ArrayList<Expenses> list = new ArrayList();
        String sql = "SELECT * FROM expenses";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Expenses expenses = Expenses.fromRS(rs);
                list.add(expenses);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Expenses> getAll(String where, String order) {
        ArrayList<Expenses> list = new ArrayList();
        String sql = "SELECT * FROM expenses where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Expenses expenses = Expenses.fromRS(rs);
                list.add(expenses);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Expenses> getAll(String order) {
        ArrayList<Expenses> list = new ArrayList();
        String sql = "SELECT * FROM expenses ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Expenses expenses = Expenses.fromRS(rs);
                list.add(expenses);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Expenses save(Expenses obj) {
        String sql = "INSERT INTO expenses (expenses_pay, user_id)"
                + "VALUES(?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getPay());
            stmt.setInt(2, obj.getUserId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }        
        return obj;
    }

    @Override
    public Expenses update(Expenses obj) {
        String sql = "UPDATE expenses"
                + " SET expenses_pay = ?, user_id = ?"
                + " WHERE expenses_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getPay());
            stmt.setInt(2, obj.getUserId());
            stmt.setInt(3, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Expenses obj) {
        String sql = "DELETE FROM expenses WHERE expenses_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    
    
    public List<ExpensesReport> getExpensesPerMonth() {
        ArrayList<ExpensesReport> list = new ArrayList();
        String sql = """
                     SELECT strftime('%Y-%m-%d', exp.expenses_date) AS Date,
                            expd.expenses_id,
                            expd.expenses_detail_name,
                            SUM(expd.expenses_detail_price) AS Price,
                            SUM(exp.expenses_pay) AS TotalPrice_Per_Month
                       FROM expenses exp
                            INNER JOIN
                            expenses_detail expd ON exp.expenses_id = expd.expenses_id
                      GROUP BY Date,
                               expd.expenses_id,
                               expd.expenses_detail_name
                      ORDER BY Date,
                               expd.expenses_id,
                               expd.expenses_detail_name;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ExpensesReport expensesReport = ExpensesReport.fromRS(rs);
                list.add(expensesReport);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ExpensesReport> getExpensesPerMonth(String begin, String end) {
        ArrayList<ExpensesReport> list = new ArrayList();
        String sql = """
                     SELECT strftime('%Y-%m-%d', exp.expenses_date) AS Date,
                                                expd.expenses_id,
                                                expd.expenses_detail_name ,
                                                SUM(expd.expenses_detail_price) AS Price,
                                                SUM(exp.expenses_pay) AS TotalPrice_Per_Month
                                           FROM expenses exp
                                                INNER JOIN
                                                expenses_detail expd ON exp.expenses_id = expd.expenses_id
                                          WHERE exp.expenses_date BETWEEN ? AND ?
                                          GROUP BY Date,
                                                    expd.expenses_id,
                                                    expd.expenses_detail_name
                                          ORDER BY Date,
                                                    expd.expenses_id,
                                                    expd.expenses_detail_name;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ExpensesReport expensesReport = ExpensesReport.fromRS(rs);
                list.add(expensesReport);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
