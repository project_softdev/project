--
-- File generated with SQLiteStudio v3.4.3 on Fri Oct 13 19:20:35 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
DROP TABLE IF EXISTS category;

CREATE TABLE category (
    category_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    category_name TEXT (50) NOT NULL
);

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         1,
                         'เครื่องดื่ม'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         2,
                         'ของหวาน'
                     );


-- Table: customer
DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
    customer_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    customer_name TEXT (50) NOT NULL,
    customer_tel  TEXT      NOT NULL
                            UNIQUE,
    member_id     INTEGER   REFERENCES member (member_id) ON DELETE CASCADE
                                                          ON UPDATE CASCADE
);

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         member_id
                     )
                     VALUES (
                         1,
                         'ตอง',
                         '0846612014',
                         1
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         member_id
                     )
                     VALUES (
                         2,
                         'บีม',
                         '0947751243',
                         NULL
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         member_id
                     )
                     VALUES (
                         3,
                         'กิ่ง',
                         '0871152220',
                         2
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         member_id
                     )
                     VALUES (
                         4,
                         'มด',
                         '0962214570',
                         NULL
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         member_id
                     )
                     VALUES (
                         5,
                         'พั้น',
                         '0875541210',
                         3
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel,
                         member_id
                     )
                     VALUES (
                         6,
                         'ฟ้า',
                         '0891236547',
                         4
                     );


-- Table: employee
DROP TABLE IF EXISTS employee;

CREATE TABLE employee (
    emp_id          INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    emp_name        TEXT (50) UNIQUE,
    emp_tel         TEXT (10) NOT NULL,
    emp_email       TEXT (50) NOT NULL,
    emp_position    TEXT (20) NOT NULL,
    emp_hourly_wage INTEGER   NOT NULL
);

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_tel,
                         emp_email,
                         emp_position,
                         emp_hourly_wage
                     )
                     VALUES (
                         1,
                         'pasinee',
                         '0812223333',
                         'pasinee@dcoffee.com',
                         'พนักงาน',
                         40
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_tel,
                         emp_email,
                         emp_position,
                         emp_hourly_wage
                     )
                     VALUES (
                         2,
                         'waraporn',
                         '0823334444',
                         'waraporn@dcoffee.com',
                         'พนักงาน',
                         40
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_tel,
                         emp_email,
                         emp_position,
                         emp_hourly_wage
                     )
                     VALUES (
                         3,
                         'phatcharin',
                         '0895564117',
                         'phatcharin@dcoffee.com',
                         'พนักงาน',
                         40
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_tel,
                         emp_email,
                         emp_position,
                         emp_hourly_wage
                     )
                     VALUES (
                         4,
                         'saranya',
                         '0845557777',
                         'saranya@dcoffee.com',
                         'พนักงาน',
                         40
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_tel,
                         emp_email,
                         emp_position,
                         emp_hourly_wage
                     )
                     VALUES (
                         5,
                         'nalinthip',
                         '0874412254',
                         'nalinthip@dcoffee.com',
                         'พนักงาน',
                         40
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_tel,
                         emp_email,
                         emp_position,
                         emp_hourly_wage
                     )
                     VALUES (
                         6,
                         'saharat',
                         '0912253336',
                         'saharat@dcoffee.com',
                         'พนักงาน',
                         40
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_tel,
                         emp_email,
                         emp_position,
                         emp_hourly_wage
                     )
                     VALUES (
                         7,
                         'pongpipat',
                         '0947771112',
                         'pongpipat@dcoffee.com',
                         'พนักงาน',
                         40
                     );

INSERT INTO employee (
                         emp_id,
                         emp_name,
                         emp_tel,
                         emp_email,
                         emp_position,
                         emp_hourly_wage
                     )
                     VALUES (
                         8,
                         'pita',
                         '0931313131',
                         'pita@dcoffee.com',
                         'ผู้จัดการ',
                         60
                     );


-- Table: material
DROP TABLE IF EXISTS material;

CREATE TABLE material (
    mat_id             INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    mat_name           TEXT (50) UNIQUE,
    mat_min            INTEGER   NOT NULL,
    mat_quantity       INTEGER   NOT NULL,
    mat_unit           TEXT (20) NOT NULL,
    mat_price_per_unit INTEGER   NOT NULL
);

INSERT INTO material (
                         mat_id,
                         mat_name,
                         mat_min,
                         mat_quantity,
                         mat_unit,
                         mat_price_per_unit
                     )
                     VALUES (
                         1,
                         'ครีมเทียม',
                         5,
                         10,
                         'กล่อง',
                         120
                     );

INSERT INTO material (
                         mat_id,
                         mat_name,
                         mat_min,
                         mat_quantity,
                         mat_unit,
                         mat_price_per_unit
                     )
                     VALUES (
                         2,
                         'นม',
                         5,
                         13,
                         'กล่อง',
                         45
                     );

INSERT INTO material (
                         mat_id,
                         mat_name,
                         mat_min,
                         mat_quantity,
                         mat_unit,
                         mat_price_per_unit
                     )
                     VALUES (
                         3,
                         'น้ำตาล',
                         5,
                         12,
                         'ถุง',
                         30
                     );

INSERT INTO material (
                         mat_id,
                         mat_name,
                         mat_min,
                         mat_quantity,
                         mat_unit,
                         mat_price_per_unit
                     )
                     VALUES (
                         4,
                         'ผงโกโก้',
                         5,
                         14,
                         'ถุง',
                         185
                     );

INSERT INTO material (
                         mat_id,
                         mat_name,
                         mat_min,
                         mat_quantity,
                         mat_unit,
                         mat_price_per_unit
                     )
                     VALUES (
                         5,
                         'นมข้นหวาน',
                         5,
                         8,
                         'กระป๋อง',
                         25
                     );

INSERT INTO material (
                         mat_id,
                         mat_name,
                         mat_min,
                         mat_quantity,
                         mat_unit,
                         mat_price_per_unit
                     )
                     VALUES (
                         6,
                         'ผงชาไทย',
                         5,
                         12,
                         'ถุง',
                         125
                     );

INSERT INTO material (
                         mat_id,
                         mat_name,
                         mat_min,
                         mat_quantity,
                         mat_unit,
                         mat_price_per_unit
                     )
                     VALUES (
                         7,
                         'ผงชาเขียว',
                         5,
                         8,
                         'ถุง',
                         180
                     );

INSERT INTO material (
                         mat_id,
                         mat_name,
                         mat_min,
                         mat_quantity,
                         mat_unit,
                         mat_price_per_unit
                     )
                     VALUES (
                         8,
                         'เมล็ดกาแฟ',
                         5,
                         7,
                         'ถุง',
                         200
                     );

INSERT INTO material (
                         mat_id,
                         mat_name,
                         mat_min,
                         mat_quantity,
                         mat_unit,
                         mat_price_per_unit
                     )
                     VALUES (
                         9,
                         'นมข้นจืด',
                         5,
                         5,
                         'กระป๋อง',
                         25
                     );

INSERT INTO material (
                         mat_id,
                         mat_name,
                         mat_min,
                         mat_quantity,
                         mat_unit,
                         mat_price_per_unit
                     )
                     VALUES (
                         10,
                         'แป้งสาลี',
                         5,
                         6,
                         'ถุง',
                         75
                     );


-- Table: member
DROP TABLE IF EXISTS member;

CREATE TABLE member (
    member_id    INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    member_name  TEXT (50) NOT NULL,
    member_tel   TEXT (10) NOT NULL,
    member_point INTEGER,
    customer_id  INTEGER   REFERENCES customer (customer_id) ON DELETE CASCADE
                                                             ON UPDATE CASCADE
);

INSERT INTO member (
                       member_id,
                       member_name,
                       member_tel,
                       member_point,
                       customer_id
                   )
                   VALUES (
                       1,
                       'ตอง',
                       '0846612014',
                       600,
                       1
                   );

INSERT INTO member (
                       member_id,
                       member_name,
                       member_tel,
                       member_point,
                       customer_id
                   )
                   VALUES (
                       2,
                       'กิ่ง',
                       '0871152220',
                       1500,
                       3
                   );

INSERT INTO member (
                       member_id,
                       member_name,
                       member_tel,
                       member_point,
                       customer_id
                   )
                   VALUES (
                       3,
                       'พั้น',
                       '0875541210',
                       400,
                       5
                   );

INSERT INTO member (
                       member_id,
                       member_name,
                       member_tel,
                       member_point,
                       customer_id
                   )
                   VALUES (
                       4,
                       'ฟ้า',
                       '0891236547',
                       100,
                       6
                   );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE product (
    product_id          INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    product_name        TEXT (50) UNIQUE,
    product_price       DOUBLE    NOT NULL,
    product_size        TEXT (5)  DEFAULT SML
                                  NOT NULL,
    product_sweet_level TEXT (5)  DEFAULT (123) 
                                  NOT NULL,
    product_type        TEXT (5)  DEFAULT HCF
                                  NOT NULL,
    category_id         INTEGER   DEFAULT (1) 
                                  NOT NULL
                                  REFERENCES category (category_id) ON DELETE SET NULL
                                                                    ON UPDATE CASCADE
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        1,
                        'กาแฟดำ',
                        40.0,
                        'SML',
                        '0123',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        2,
                        'ชาเขียว',
                        45.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        3,
                        'โกโก้',
                        40.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        4,
                        'โปรตีนเชค',
                        70.0,
                        '-',
                        '0123',
                        '-',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        5,
                        'อเมริกาโน่',
                        50.0,
                        'SML',
                        '0123',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        6,
                        'มอคค่า',
                        60.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        7,
                        'ลาเต้',
                        50.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        8,
                        'คาปูชิโน่',
                        60.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        9,
                        'นมสด',
                        50.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        10,
                        'ชาเขียวนม',
                        55.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        11,
                        'กาแฟส้ม',
                        65.0,
                        'SML',
                        '0123',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        12,
                        'มัทฉะลาเต้',
                        60.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        13,
                        'ชาน้ำผึ้งมะนาว',
                        50.0,
                        'SML',
                        '0123',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        14,
                        'ชาพีช',
                        45.0,
                        'SML',
                        '0123',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        15,
                        'ชาเบอรี่',
                        50.0,
                        'SML',
                        '0123',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        16,
                        'น้ำส้ม',
                        55.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        17,
                        'น้ำเสาวรสโซดา',
                        60.0,
                        'SML',
                        '0123',
                        '-',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        18,
                        'นมน้ำผึ้ง',
                        55.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        19,
                        'นมหมี',
                        60.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        20,
                        'น้ำแตงโม',
                        55.0,
                        'SML',
                        '0123',
                        'CF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        21,
                        'น้ำมะนาว',
                        55.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        22,
                        'น้ำกีวี่',
                        60.0,
                        'SML',
                        '0123',
                        'CF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        23,
                        'น้ำมะพร้าว',
                        60.0,
                        'SML',
                        '0123',
                        'CF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        24,
                        'ฮันนี่โกลด์มะม่วง',
                        45.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        25,
                        'แพนเค้กเรดเวลเวท',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        26,
                        'แพนเค้กกล้วยหอม',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        27,
                        'แพนเค้กราสป์เบอร์รี่',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        28,
                        'วาฟเฟิลชาเขียว',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        29,
                        'วาฟเฟิลไอศกรีม',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        30,
                        'ครีมบูเล่เสาวรส',
                        35.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        31,
                        'พุดดิ้งขนมปัง',
                        40.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        32,
                        'ช็อกโกแลตลาวา',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        33,
                        'ช็อกโกแลตชีสเค้ก',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        34,
                        'มาการอง',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        35,
                        'ชีสพายชาเขียว',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        36,
                        'มูสเค้กช็อกโกแลต',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        37,
                        'ชีสเค้ก',
                        30.0,
                        '-',
                        '-',
                        '-',
                        2
                    );


-- Table: promotion
DROP TABLE IF EXISTS promotion;

CREATE TABLE promotion (
    promotion_id    INTEGER PRIMARY KEY ASC AUTOINCREMENT,
    promotion_name  TEXT    UNIQUE
                            NOT NULL,
    promotion_start TEXT    NOT NULL,
    promotion_end   TEXT    NOT NULL
);


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    user_id       INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    user_login    TEXT (50) UNIQUE,
    user_gender   TEXT (3)  NOT NULL,
    user_password TEXT (50) NOT NULL,
    user_role     INTEGER   NOT NULL,
    user_name     TEXT (50),
    emp_id        INTEGER   REFERENCES employee (emp_id) ON DELETE SET NULL
                                                         ON UPDATE CASCADE
);

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name,
                     emp_id
                 )
                 VALUES (
                     1,
                     'pasinee',
                     'F',
                     'password',
                     1,
                     'Pasinee',
                     1
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name,
                     emp_id
                 )
                 VALUES (
                     2,
                     'phatcharin',
                     'F',
                     'password',
                     1,
                     'Phatcharin',
                     3
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name,
                     emp_id
                 )
                 VALUES (
                     3,
                     'saraya',
                     'F',
                     'password',
                     1,
                     'Saraya',
                     4
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name,
                     emp_id
                 )
                 VALUES (
                     4,
                     'waraporn',
                     'F',
                     'password',
                     1,
                     'Waraporn',
                     2
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name,
                     emp_id
                 )
                 VALUES (
                     5,
                     'nalinthip',
                     'F',
                     'password',
                     1,
                     'Nalinthip',
                     5
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name,
                     emp_id
                 )
                 VALUES (
                     6,
                     'pongpipat',
                     'M',
                     'password',
                     1,
                     'Pongpipat',
                     7
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name,
                     emp_id
                 )
                 VALUES (
                     7,
                     'Saharat',
                     'M',
                     'password',
                     1,
                     'Saharat',
                     6
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name,
                     emp_id
                 )
                 VALUES (
                     8,
                     'settha',
                     'M',
                     'password',
                     2,
                     'Settha',
                     NULL
                 );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
