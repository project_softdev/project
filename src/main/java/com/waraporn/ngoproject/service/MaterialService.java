/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.MaterialDao;
import com.waraporn.ngoproject.model.Material;
import com.waraporn.ngoproject.report.MaterialReport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class MaterialService {
    public Material getById(int id) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.get(id);
    }
    
    public ArrayList<Material> getByName() {
        MaterialDao materialDao = new MaterialDao();
        return (ArrayList<Material>) materialDao.getAll(" mat_name asc");
    }
    
    public List<Material> getMaterials(){
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getAll();
    }
    
    public Material getId(int id) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.get(id);
                
    }

    public Material addNew(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.save(editedMaterial);
    }

    public Material update(Material editedMaterial) {
       if (editedMaterial != null) {
            MaterialDao matDao = new MaterialDao();
            return matDao.update(editedMaterial);
        } else {
            System.err.println("Cannot update a null material.");
            return null;
        }
    }

    public int delete(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.delete(editedMaterial);
    }
    
    public List<MaterialReport> getTopTenLowQuantity() {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getMaterialRemainingQuantity(10);
    }

    public List<MaterialReport> getTopTenLowQuantity(String begin, String end) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getMaterialRemainingQuantity(begin, end, 10);
    }
}
