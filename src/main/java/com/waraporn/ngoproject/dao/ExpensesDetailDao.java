/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.dao;

import com.waraporn.ngoproject.databasehelper.DatabaseHelper;
import com.waraporn.ngoproject.model.BillDetail;
import com.waraporn.ngoproject.model.ExpensesDetail;
import com.waraporn.ngoproject.model.ExpensesDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class ExpensesDetailDao implements Dao<ExpensesDetail> {

    @Override
    public ExpensesDetail get(int id) {
        ExpensesDetail expensesDetail = null;
        String sql = "SELECT * FROM expenses_detail WHERE expenses_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                expensesDetail = ExpensesDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return expensesDetail;
    }

    public List<ExpensesDetail> getByExpensesId(int id) {
        ArrayList<ExpensesDetail> list = new ArrayList();
        String sql = "SELECT * FROM expenses_detail WHERE expenses_detail_id=" + id + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ExpensesDetail expensesDetail = ExpensesDetail.fromRS(rs);
                list.add(expensesDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

//    public ExpensesDetail getById(int id) {
//        ExpensesDetail expensesDetail = null;
//        String sql = "SELECT * FROM expenses_detail WHERE expenses_detail_id=?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, id);
//            ResultSet rs = stmt.executeQuery();
//
//            while (rs.next()) {
//                expensesDetail = expensesDetail.fromRS(rs);
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return expensesDetail;
//    }
    public List<ExpensesDetail> getByCheckExpensesId(int id) {
        ArrayList<ExpensesDetail> list = new ArrayList();
        String sql = "SELECT * FROM expenses_detail WHERE expenses_id= " + id + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ExpensesDetail expensesDetail = ExpensesDetail.fromRS(rs);
                list.add(expensesDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<ExpensesDetail> getAll() {
        ArrayList<ExpensesDetail> list = new ArrayList();
        String sql = "SELECT * FROM expenses_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ExpensesDetail expensesDetail = ExpensesDetail.fromRS(rs);
                list.add(expensesDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<ExpensesDetail> getAll(String where, String order) {
        ArrayList<ExpensesDetail> list = new ArrayList();
        String sql = "SELECT * FROM expenses_detail where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ExpensesDetail expensesDetail = ExpensesDetail.fromRS(rs);
                list.add(expensesDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ExpensesDetail> getAll(String order) {
        ArrayList<ExpensesDetail> list = new ArrayList();
        String sql = "SELECT * FROM expenses_detail ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ExpensesDetail expensesDetail = ExpensesDetail.fromRS(rs);
                list.add(expensesDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ExpensesDetail save(ExpensesDetail obj) {
        String sql = "INSERT INTO expenses_detail (expenses_detail_name, expenses_detail_price, expenses_id)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2, obj.getPrice());
            stmt.setInt(3, obj.getExpensesId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ExpensesDetail update(ExpensesDetail obj) {
        String sql = "UPDATE expenses_detail"
                + " SET expenses_detail_name = ?, expenses_detail_price = ?, expenses_id = ?"
                + " WHERE expenses_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2, obj.getPrice());
            stmt.setInt(3, obj.getExpensesId());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ExpensesDetail obj) {
        String sql = "DELETE FROM expenses_detail WHERE expenses_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
