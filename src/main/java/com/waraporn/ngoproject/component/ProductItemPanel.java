/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.waraporn.ngoproject.component;

import com.waraporn.ngoproject.member.AddMemberDialog;
import com.waraporn.ngoproject.model.Product;
import com.waraporn.ngoproject.pos.OptionProductDialog;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author ACER
 */
public class ProductItemPanel extends javax.swing.JPanel implements BuyProductable{
    
    private final Product product;
    private ArrayList<BuyProductable> subscribers = new ArrayList<>();

    /**
     * Creates new form ProductItemPanel
     */
    public ProductItemPanel(Product p) {
        initComponents();
        product = p;
        lblName.setText(product.getName());
        lblPrice.setText("ราคา " + product.getPrice() + " บาท");
        ImageIcon icon = new ImageIcon("./pd" + product.getId() + ".png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) (150 * (float) width / height), 144, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        imgProduct.setIcon(icon);
        imgProduct.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                OpenOptionProductDialog();
//                for (BuyProductable s : subscribers) {
//                    s.buy(product, Integer.parseInt(txtQty.getText()));
//                }
                //txtQty.setText("1");
            }
            
        });
        
    }
    
    public void addOnByProduct(BuyProductable subscriber) {
        subscribers.add(subscriber);
    }
    
    private void OpenOptionProductDialog() {
        JFrame jframe = (JFrame) SwingUtilities.getRoot(this);
        OptionProductDialog optionProductDialog = new OptionProductDialog(jframe, product);
        optionProductDialog.setLocationRelativeTo(this);
        optionProductDialog.addOnByProduct(this);
        optionProductDialog.setVisible(true);
        optionProductDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                
            }

        });

    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        imgProduct = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        lblPrice = new javax.swing.JLabel();

        imgProduct.setBackground(new java.awt.Color(204, 255, 204));
        imgProduct.setOpaque(true);
        imgProduct.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imgProductMouseClicked(evt);
            }
        });

        lblName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblName.setText("ProductName");

        lblPrice.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblPrice.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPrice.setText("Price");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblPrice, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(imgProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(imgProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPrice)
                .addContainerGap(12, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void imgProductMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imgProductMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_imgProductMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel imgProduct;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPrice;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int qty) {
        for (BuyProductable s : subscribers) {
            s.buy(product, qty);
        }
    }
}
