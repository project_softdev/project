/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.MemberDao;
import com.waraporn.ngoproject.model.Member;
import java.util.List;

/**
 *
 * @author ACER
 */
public class MemberService {
    public List<Member> getMembers() {
        MemberDao memberDao = new MemberDao();
        return memberDao.getAll(" member_id asc");
    }

    public Member addNew(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.save(editedMember);
    }

    public Member update(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.update(editedMember);
    }

    public int delete(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.delete(editedMember);
    }

    public Member getById(int id) {
        MemberDao memberDao = new MemberDao();
        return memberDao.get(id);
    }
}
