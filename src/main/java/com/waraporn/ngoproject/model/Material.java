/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class Material {
    private int id;
    private String name;
    private int min;
    private int qty;
    private String unit;
    private int price;
    private String status;

    public Material(int id, String name, int min, int qty, String unit, int price, String status) {
        this.id = id;
        this.name = name;
        this.min = min;
        this.qty = qty;
        this.unit = unit;
        this.price = price;
        this.status = status;
    }
    
    public Material(String name, int min, int qty, String unit, int price, String status) {
        this.id = -1;
        this.name = name;
        this.min = min;
        this.qty = qty;
        this.unit = unit;
        this.price = price;
        this.status = status;
    }
    
    public Material() {
        this.id = -1;
        this.name = "";
        this.min = 0;
        this.qty = 0;
        this.unit = "";
        this.price = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", min=" + min + ", qty=" + qty + ", unit=" + unit + ", price=" + price + ", status=" + status + '}';
    }
    
    
    
    public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("mat_id"));
            material.setName(rs.getString("mat_name"));
            material.setMin(rs.getInt("mat_min"));
            material.setQty(rs.getInt("mat_quantity"));
            material.setUnit(rs.getString("mat_unit"));
            material.setPrice(rs.getInt("mat_price_per_unit"));
            material.setStatus(rs.getString("mat_status"));
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }
}
