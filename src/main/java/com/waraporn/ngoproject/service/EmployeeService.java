/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.model.Employee;
import com.waraporn.ngoproject.dao.EmployeeDao;
import java.util.ArrayList;

import java.util.List;

/**
 *
 * @author patch
 */
public class EmployeeService {

    public Employee getById(int id) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.get(id);
    }

//    public Employee getName(String name) {
//        EmployeeDao employeeDao = new EmployeeDao();
//        return employeeDao.get(name);
//    }
    public List<Employee> getEmployee() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(" emp_id asc");
    }

    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }
}
