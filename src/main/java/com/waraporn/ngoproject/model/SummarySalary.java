/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.model;

import com.waraporn.ngoproject.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Time;

/**
 *
 * @author DELL
 */
public class SummarySalary {

    private int id;
    private String date;
    private int workhour;
    private float salary;
    private int emId;
    private Employee employee;

    public SummarySalary(int id, String date, int workhour, float salary, int emId) {
        this.id = id;
        this.date = date;
        this.workhour = workhour;
        this.salary = salary;
        this.emId = emId;
    }

    public SummarySalary() {
        this.id = -1;
        this.date = date;
        this.workhour = 0;
        this.salary = 0.0f;
        this.emId = -1;
    }

    public SummarySalary(String date, int workhour, float salary, int emId, Employee employee) {
        this.id = - 1;
        this.date = date;
        this.workhour = workhour;
        this.salary = salary;
        this.emId = emId;
        this.employee = employee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getWorkhour() {
        return workhour;
    }

    public void setWorkhour(int workhour) {
        this.workhour = workhour;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public int getEmId() {
        return emId;
    }

    public void setEmId(int emId) {
        this.emId = emId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public static SummarySalary fromRS(ResultSet rs) {
        SummarySalary summarysalary = new SummarySalary();
        try {
            summarysalary.setId(rs.getInt("ss_id"));
            summarysalary.setDate(rs.getString("ss_date"));
            summarysalary.setWorkhour(rs.getInt("ss_work_hour"));
            summarysalary.setSalary(rs.getFloat("ss_salary"));
            summarysalary.setEmId(rs.getInt("employee_id"));

            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(summarysalary.getEmId());
            summarysalary.setEmployee(employee);
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return summarysalary;
    }

}
