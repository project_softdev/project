/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.dao;

import com.waraporn.ngoproject.databasehelper.DatabaseHelper;
import com.waraporn.ngoproject.model.Material;
import com.waraporn.ngoproject.report.MaterialReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class MaterialDao implements Dao<Material> {

    @Override
    public Material get(int id) {
        Material material = null;
        String sql = "SELECT * FROM material WHERE mat_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                material = Material.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return material;
    }

    @Override
    public List<Material> getAll() {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM material";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material material = Material.fromRS(rs);
                list.add(material);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Material> getAll(String where, String order) {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM material where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material material = Material.fromRS(rs);
                list.add(material);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Material> getAll(String order) {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM material  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material material = Material.fromRS(rs);
                list.add(material);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Material save(Material obj) {
        String sql = "INSERT INTO material(mat_name, mat_min, mat_quantity, mat_unit, mat_price_per_unit)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getMin());
            stmt.setInt(3, obj.getQty());
            stmt.setString(4, obj.getUnit());
            stmt.setInt(5, obj.getPrice());
            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Material update(Material obj) {
        String sql = "UPDATE material "
                + "SET mat_name = ?, mat_min = ?, mat_quantity = ?, mat_unit = ?, mat_price_per_unit = ? "
                + "WHERE mat_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getMin());
            stmt.setInt(3, obj.getQty());
            stmt.setString(4, obj.getUnit());
            stmt.setFloat(5, obj.getPrice());
            stmt.setInt(6, obj.getId());
            int ret = stmt.executeUpdate();
            updateStatus(obj.getId(), conn);
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    private void updateStatus(int id, Connection conn) throws SQLException {
        String sql = "UPDATE material SET mat_status = "
                + "CASE "
                + "WHEN mat_quantity >= mat_min THEN 'คงเหลือ' "
                + "WHEN mat_quantity < mat_min AND mat_quantity > 0 THEN 'ใกล้หมด' "
                + "ELSE 'หมด' "
                + "END "
                + "WHERE mat_id = ?";

        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, id);
        stmt.executeUpdate();
    }

    @Override
    public int delete(Material obj) {
        String sql = "DELETE FROM material WHERE mat_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<MaterialReport> getMaterialRemainingQuantity(int limit) {
        ArrayList<MaterialReport> list = new ArrayList();
        String sql = """
                     SELECT mat.mat_id,
                               mat.mat_name,
                               mat.mat_min,
                               SUM(cmd.old_qty) AS OldQuantity,
                               mat.mat_quantity ,
                               SUM(cmd.old_qty-mat_quantity) AS UsedQuantity,
                               mat.mat_status 
                          FROM checkMatHistory cm
                               INNER JOIN
                               checkMatHistory_detail cmd ON cm.cmh_id = cmd.cmh_id
                               INNER JOIN
                               material mat ON cmd.mat_id = mat.mat_id
                         GROUP BY mat.mat_id,
                                  mat.mat_name,
                                  mat.mat_quantity,
                                  mat.mat_status
                     
                         ORDER BY mat.mat_quantity ASC
                         LIMIT ?;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                MaterialReport materialReport = MaterialReport.fromRS(rs);
                list.add(materialReport);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<MaterialReport> getMaterialRemainingQuantity(String begin, String end, int limit) {
        ArrayList<MaterialReport> list = new ArrayList();
        String sql = """
                     SELECT mat.mat_id,
                               mat.mat_name,
                               mat.mat_min,
                               SUM(cmd.old_qty) AS OldQuantity,
                               mat.mat_quantity ,
                               SUM(cmd.old_qty-mat_quantity) AS UsedQuantity,
                               mat.mat_status 
                          FROM checkMatHistory cm
                               INNER JOIN
                               checkMatHistory_detail cmd ON cm.cmh_id = cmd.cmh_id
                               INNER JOIN
                               material mat ON cmd.mat_id = mat.mat_id
                               AND cm.cmh_date BETWEEN ? AND ?
                         GROUP BY mat.mat_id,
                                  mat.mat_name,
                                  mat.mat_quantity,
                                  mat.mat_status
                         ORDER BY mat.mat_quantity ASC
                         LIMIT ?;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                MaterialReport materialReport = MaterialReport.fromRS(rs);
                list.add(materialReport);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
