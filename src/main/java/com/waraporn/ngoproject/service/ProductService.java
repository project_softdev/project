/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.service;

import com.waraporn.ngoproject.dao.ProductDao;
import com.waraporn.ngoproject.model.Product;
import com.waraporn.ngoproject.report.ProductReport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class ProductService {
    
    public Product getById(int id) {
        ProductDao productDao = new ProductDao();
        return productDao.get(id);
    }
    
    public ArrayList<Product> getByName() {
        ProductDao productDao = new ProductDao();
        return (ArrayList<Product>) productDao.getAll(" product_name asc");
    }
    
    public List<Product> getProducts(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" product_id asc");
    }

    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
         if (editedProduct != null) {
            ProductDao proDao = new ProductDao();
            return proDao.update(editedProduct);
        } else {
            System.err.println("Cannot update a null product.");
            return null;
        }
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }
    
    public List<ProductReport> getTopTenProductByTotalPrice() {
        ProductDao productDao = new ProductDao();
        return productDao.getProductTopSeller(10);
    }

    public List<ProductReport> getTopTenProductByTotalPrice(String begin, String end) {
        ProductDao productDao = new ProductDao();
        return productDao.getProductTopSeller(begin, end, 10);
    }
}
