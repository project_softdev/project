/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.ngoproject.dao;

import com.waraporn.ngoproject.databasehelper.DatabaseHelper;
import com.waraporn.ngoproject.model.Bill;
import com.waraporn.ngoproject.model.Bill;
import com.waraporn.ngoproject.model.Bill;
import com.waraporn.ngoproject.model.BillDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class BillDao implements Dao<Bill>{

    @Override
    public Bill get(int id) {
        Bill bill = null;
        String sql = "SELECT * FROM bill WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                bill = Bill.fromRS(rs);
                BillDetailDao bdd = new BillDetailDao();
                ArrayList<BillDetail> billDetails = (ArrayList<BillDetail>)bdd.getAll("bill_id=" + bill.getId()," bill_detail_id");
                bill.setBillDetails(billDetails);      
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return bill;
    }

    @Override
    public List<Bill> getAll() {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Bill> getAll(String where, String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Bill> getAll(String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Bill save(Bill obj) {

        String sql = "INSERT INTO bill (bill_shop_name, bill_subtotal, bill_discount, bill_total_price, bill_total_qty, user_id)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getShopName());
            stmt.setFloat(2, obj.getSubtotal());
            stmt.setFloat(3, obj.getDiscount());
            stmt.setFloat(4, obj.getTotalPrice());
            stmt.setInt(5, obj.getTotalQty());
            stmt.setInt(6, obj.getUserId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }        
        return obj;
    }

    @Override
    public Bill update(Bill obj) {
        String sql = "UPDATE bill"
                + " SET bill_shop_name = ?, bill_subtotal = ?, bill_discount = ?, bill_total_price = ?, bill_total_qty = ?, user_id = ?"
                + " WHERE bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getShopName());
            stmt.setFloat(2, obj.getSubtotal());
            stmt.setFloat(3, obj.getDiscount());
            stmt.setFloat(4, obj.getTotalPrice());
            stmt.setInt(5, obj.getTotalQty());
            stmt.setInt(6, obj.getUserId());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Bill obj) {
        String sql = "DELETE FROM bill WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    
}
